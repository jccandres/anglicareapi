#Anglicare API

1. [API Documentation](#markdown-header-api-documentation)
1. [Authentication](#markdown-header-authentication)
1. [Test Data](#markdown-header-test-data)
1. [Register a new user](#markdown-header-register-a-new-user)
1. [Get Client Info](#markdown-header-get-client-info)
1. [Register PIN](#markdown-header-register-pin)
1. [Validate PIN](#markdown-header-validate-pin)
1. [Update Firebase Key](#markdown-header-update-firebase-key)
1. [Get Your Schedule](#markdown-header-get-your-schedule)
1. [Change Visit](#markdown-header-change-visit)
1. [Cancel Visit](#markdown-header-cancel-visit)
1. [Request Visit](#markdown-header-request-visit)
1. [Get Your Request](#markdown-header-get-your-request)
1. [Request Transport](#markdown-header-request-transport)
1. [Unlink User to a Device](#markdown-header-unlink-user)
1. [Update Notification](#markdown-header-update-notification)
1. [Contact/Send Message](#markdown-header-send-message)
1. [Get Notification](#markdown-header-get-notification)
1. [Remove Notification](#markdown-header-remove-notification)
1. [Get Number of Unread Notification](#markdown-header-get-number-of-unread-notification)
1. [Accept Notification](#markdown-header-accept-notification)

---

## API Documentation
Development: `https://api.arv.org.au/AnglicareAPI/api`  

---

### Authentication
#### Basic Auth

Authorization: `Basic YWRtaW46MTIzNA==` / `Basic b3B0aW1pbmQ6YW5nbGljYXJlQVBJ`

x-api-key: `bWCxzkqRY+qqQkPiNkV2ww====`

---

### Test Data
Requests with '?limit=n' will return n results


If '?limit=n' is ommitted, the api will return 50 results

#### Clients

GET `/clients`

##### Response
```json
200 OK
{
    "result": {
        "data": [
            {
                "ClientID": "4C5EE703-7F8C-4FBA-BD27-00039284C246",
                "FirstName": "Fulvio",
                "LastName": "Fante (Finished)",
                "Telephone1": "91301369",
                "Telephone2": "",
                "Telephone3": "",
                "DateOfBirth": "1929-08-21 00:00:00.000",
                "carerToken": "4C33214E-D552-4237-8C25-C542C4601807"
            },
            {
                "ClientID": "C60F4F43-F60F-4B89-ABE6-0003E74B04F5",
                "FirstName": "Jennifer",
                "LastName": "Stanbury",
                "Telephone1": "9487 1085",
                "Telephone2": "",
                "Telephone3": "NS-NW-HA",
                "DateOfBirth": "1937-03-21 00:00:00.000",
                "carerToken": "43210CB1-888D-4D4F-A60A-1E7CB807F446"
            },
            {
                "ClientID": "E7ABEFD5-B39C-4536-B50E-000592659119",
                "FirstName": "Catherine",
                "LastName": "Daniel",
                "Telephone1": "47048598",
                "Telephone2": "",
                "Telephone3": "ZZZ-HA",
                "DateOfBirth": "1937-01-16 00:00:00.000",
                "carerToken": "C8CA04AA-4444-4FE5-A602-2CE71ADA4776"
            }
        ]
    }
}
```

#### Carers

GET `/carers/?carerToken=4C33214E-D552-4237-8C25-C542C4601807`

##### Response
```json
200 OK
{
    "result": {
        "data": [
            {
                "ContactType": "1",
                "FirstName": "Cindy",
                "LastName": "Newey",
                "Telephone1": "",
                "Telephone2": "0421565217",
                "Telephone3": ""
            }
        ]
    }
}
```

---

### Register A New User

POST `/users/RegisterUser`

##### Parameters
|     Name         |    Type    |    Description    | Sample Data (Carer)   | Sample Data (Client) |
| ---------------- | ---------- | ----------------- | ----------------- | ----------------- |
|      FirstName   |   string   |  No description   |       Toby       | Valerie          |
|      LastName    |   string   |  No description   |       Son       | Newton-John          |
|      Phone       |   string   |  No description   |     0414670214    | 0410 370 070         |
|      DateOfBirth |   string   |  No description   |     1934-06-12    | 1934-06-12        |
|  ClientFirstName |   string   |  No description   |     Valerie        |                   |
|    ContactType   |   string   |  Client or Carer  |       Carer       | Client            |

##### Response
```json
200 OK
{
    "result": {
        "status": true,
        "data": {
            "userToken": "A845C54531DF4AEE95F360E8814D040E",
            "clientToken": "4C5EE703-7F8C-4FBA-BD27-00039284C246"
        }
    }
}
```

---

### Get Client Info

GET `/client?clientToken=C60F4F43-F60F-4B89-ABE6-0003E74B04F5`

##### Response
```json
200 OK
{
    "result": {
        "status": true,
        "data": {
            "ClientID": "C60F4F43-F60F-4B89-ABE6-0003E74B04F5",
            "Title": "Mrs",
            "FirstName": "Jennifer",
            "LastName": "Stanbury",
            "Address": "10 Rutland Place",
            "Town": "North Wahroonga",
            "County": "New South Wales",
            "PostCode": "2076      ",
            "Home": "0423456456",
            "Mobile": "0423456456",
            "emailaddress": "",
            "Status": "1",
            "SupervisorName": "Annette Lee",
            "SupervisorPhone": "8886 9400",
            "SupervisorMobile": "",
            "ClientContacts": [
                {
                    "ContactRef": "43210CB1-888D-4D4F-A60A-1E7CB807F446",
                    "Relationship": "husband",
                    "ContactType": "NOK",
                    "ContactName": "Craig Stanbury",
                    "ContactPhone": "9484 1085",
                    "ContactMobile": "",
                    "PrimaryCarer": false
                },
                {
                    "ContactRef": "8D7B2289-0DD5-4DE4-8EC5-6736F7DAEC8A",
                    "Relationship": "Daughter",
                    "ContactType": "NOK",
                    "ContactName": "Alison Lyon",
                    "ContactPhone": "0416 276276",
                    "ContactMobile": "",
                    "PrimaryCarer": true
                }
            ],
            "Services": [
                "Transport",
                "Social Support"
            ]
        }
    }
}
```

---

### Register PIN

PUT /users/pin

##### Parameters
|     Name         |    Type    |    Description    |
| ---------------- | ---------- | ----------------- |
| userToken        |   string   |  No description   |
| PIN              |   string   |  No description   |
| deviceID         |   string   |  No description   |
| deviceName       |   string   |  No description   |
| firebaseKey      |   string   |  No description   |
| notification     |   int      |  1 or 0           |
| clientToken      |  string    |  No description   |

##### Response
```json
{
    "result": {
        "status": true
        "BookingType": [
            "Personal Care",
            "Social Support",
            "Transport"
        ],
        "RegisteredNumber": "0431927400"
    }
}
```

---

### Validate PIN

POST /users/pin

##### Parameters
|     Name         |    Type    |    Description    |
| ---------------- | ---------- | ----------------- |
| userToken        |   string   |  No description   |
| PIN              |   string   |  No description   |
| clientToken      |   string   |  No description   |

##### Response
```json
{
    "result": {
        "status": true
        "BookingType": [
            "Personal Care",
            "Social Support",
            "Transport"
        ],
        "RegisteredNumber": "0431927400"
    }
}
```

---

### Update Firebase Key

PUT /users/firebaseKey

##### Parameters
|     Name         |    Type    |    Description    |
| ---------------- | ---------- | ----------------- |
| deviceID         |   string   |  No description   |
| firebaseKey      |   string   |  No description   |

##### Response
```json
{
    "result": {
        "status": true
    }
}
```

---

#### Get Your Schedule

TODO: Get all schedule within 14 days from today of a specific client

POST `/schedule`

##### Parameters
|     Name         |    Type    |    Description    |  Sample Data  |
| ---------------- | ---------- | ----------------- | ------------- |
| ClientToken      | string     | No Description    | C60F4F43-F60F-4B89-ABE6-0003E74B04F5  |
| UserToken        | string     | No Description    | 3DF7049738CB2CDFD87ACFC8E8A0A72E |

##### Response
```json
200 OK
{
    "result": {
        "status": true,
        "data": [
            {
                "VisitID": "02144955-18FB-4054-9BDE-8ABAC96978A6",
                "FirstName": "Andreas",
                "LastName": "Nicolaou",
                "StartTime": "2017-10-20 20:00:00.000",
                "VisitDescription": "Personal Services- CW Sherin",
                "Worker": "Lovely Care",
                "Duration": "30",
                "Date": "Friday, 20 October 2017 ",
                "Time": "08:00 pm",
                "Status": "Pending"
            },
            {
                "VisitID": "E1F693FA-B931-48AC-96B0-DA96E4CA5885",
                "FirstName": "Andreas",
                "LastName": "Nicolaou",
                "StartTime": "2017-10-20 20:00:00.000",
                "VisitDescription": "Personal Services- CW Mag",
                "Worker": "Lovely Care",
                "Duration": "30",
                "Date": "Friday, 20 October 2017 ",
                "Time": "08:00 pm",
                "Status": ""
            }
        ]
    }
}
```

---

#### Change Visit

POST `/schedule/change`

##### Parameters
|     Name         |    Type    |    Description    |  Sample Data  |
| ---------------- | ---------- | ----------------- | ------------- |
| userID           | string     | No Description    | F59F001361B62958E551B9702F2C6B25  |
| visitID          | string     | No Description    | 3BFA350E-704D-4AE1-9328-51D3F5236670  |
| visitDescription | string     | No Description    | DA    |
| date             | date       | No Description    | Friday, 14 July 2017  |
| time             | time       | No Description    | 02:00 pm  |
| duration         | int        | No Description    | 90    |

##### Response
```json
{
    "result": {
        "status": true
    }
}
```

---

#### Cancel Visit

POST `/schedule/cancel`

##### Parameters
|     Name         |    Type    |    Description    |  Sample Data  |
| ---------------- | ---------- | ----------------- | ------------- |
| userID           | string     | No Description    | F59F001361B62958E551B9702F2C6B25  |
| visitID          | string     | No Description    | 3BFA350E-704D-4AE1-9328-51D3F5236670  |

##### Response
```json
{
    "result": {
        "status": true
    }
}
```

---

#### Request Visit

POST `/request/visit`

##### Parameters
|     Name         |    Type    |    Description    |  Sample Data  |
| ---------------- | ---------- | ----------------- | ------------- |
| userID           | string     | No Description    | F59F001361B62958E551B9702F2C6B25  |
| visitDescription | string     | No Description    | DA    |
| date             | date       | No Description    | Friday, 14 July 2017  |
| time             | time       | No Description    | 02:00 pm  |
| duration         | int        | No Description    | 90    |

##### Response
```json
{
    "result": {
        "status": true
    }
}
```

---

#### Get Your Request

POST `/request/list`

##### Parameters
|     Name         |    Type    |    Description    |  Sample Data  |
| ---------------- | ---------- | ----------------- | ------------- |
| userToken           | string     | No Description    | 83DA8CE7BCE9740CCE071B1675E03CC2  |

##### Response
```json
200 OK
{
    "result": {
        "status": true,
        "data": [
            {
                "ID": "0C86C6BB-4AFF-4731-A2E2-F8D441E5BFF9",
                "UserID": "83DA8CE7BCE9740CCE071B1675E03CC2",
                "Date": "Monday, 06 November 2017",
                "Time": "08:00 am",
                "DateSubmitted": "Monday, 06 November 2017",
                "PreferredDate": "Monday, 06 November 2017",
                "PreferredTime": "08:00 am",
                "PickupSuburb": "Suburb",
                "DropoffSuburb": "Suburb",
                "ReturnTrip": "Yes",
                "RequestNo": "",
                "RequestStatus": "",
                "RequestType": "Request Transport",
                "CreatedAt": "2017-11-06 15:43:54.000"
            },
            {
                "ID": "C84D37FF-6A5E-4264-B2B8-E72494E693F4",
                "UserID": "83DA8CE7BCE9740CCE071B1675E03CC2",
                "Date": "Thursday, 15 September 2016",
                "Time": "03:00 pm",
                "Duration": "2 hrs 00 mins",
                "DateSubmitted": "Saturday, 28 October 2017",
                "VisitDescription": "xSupport Services\nCooking and social.",
                "RequestNo": "31665",
                "RequestStatus": "Open",
                "RequestType": "Request Cancel",
                "CreatedAt": "2017-10-28 08:48:15.000"
            },
            {
                "ID": "EC9A13DF-0101-49D0-B360-30F2CAE26ADF",
                "UserID": "83DA8CE7BCE9740CCE071B1675E03CC2",
                "Date": "Friday, 14 July 2017",
                "Time": "02:00 pm",
                "PrevDate": "Thursday, 15 September 2016",
                "PrevTime": "03:00 pm",
                "Duration": "1 hr 30 mins",
                "DateSubmitted": "Tuesday, 14 November 2017",
                "VisitDescription": "xSupport Services\nCooking and social.",
                "RequestNo": "Allocating",
                "RequestStatus": "Open",
                "RequestType": "Request Change",
                "CreatedAt": "2017-11-14 06:28:59.000"
            },
            {
                "ID": "1CD61505-0C27-480B-B675-3A8C76979248",
                "UserID": "83DA8CE7BCE9740CCE071B1675E03CC2",
                "Date": "Friday, 21 July 2017",
                "Time": "05:00:00.0000000",
                "Duration": "2 hrs",
                "DateSubmitted": "Friday, 27 October 2017",
                "VisitDescription": "DA2",
                "RequestNo": "",
                "RequestStatus": "",
                "RequestType": "Request Visit",
                "CreatedAt": "2017-10-27 08:48:28.000"
            }
        ]
    }
}
```

---

#### Request Transport

POST `/request/transport`

##### Parameters
|     Name                 | Type   |    Description       |  Sample Data  |
| ------------------------ | ------ | -------------------- | ------------- |
| UserID                   | string | No Description       | 83DA8CE7BCE9740CCE071B1675E03CC2   |
| ContactNo                | string | No Description       | 1234567    |
| TransportDate            | date   | No Description       | 2017-11-06 |
| PickupTime               | time   | No Description       | 08:00  |
| ForAppointment           | bool   | true = 1 / false = 0 | 1  |
| AppointmentTime          | time   | No Description       | 09:00  |
| AppointmentPerson        | string | No Description       | Dr. Sir Vanleor    |
| PersonsTravelingWith     | int    | No Description       | 3  |
| EquipmentToBeTransported | string | No Description       | Wheelchair |
| AssistanceRequire        | string | No Description       | Lorem Ipsum    |
| PickupStreetAddress      | string | No Description       | Street Address |
| PickupSuburb             | string | No Description       | Suburb |
| PickupAddressInfo        | string | No Description       | Lorem Ipsum    |
| DropoffStreetAddress     | string | No Description       | Street Address |
| DropoffSuburb            | string | No Description       | Suburb |
| DropoffAddressInfo       | string | No Description       | Lorem Ipsum    |
| ReturnTrip               | bool   | true = 1 / false = 0 | 1  |
| ReturnTime               | time   | No Description       | 13:00  |
| ReturnStreetAddress      | string | No Description       | Street Address |
| ReturnSuburb             | string | No Description       | Suburb |
| ReturnAddressInfo        | string | No Description       | Lorem Ipsum    |

##### Response
```json
{
    "result": {
        "status": true
    }
}
```

---

### Unlink User

PUT /users/unlink

##### Parameters
|     Name         |    Type    |    Description    |
| ---------------- | ---------- | ----------------- |
| userToken         |   string   |  No description   |
| deviceID      |   string   |  No description   |

##### Response
```json
{
    "result": {
        "status": true
    }
}
```

---

### Update Notification

PUT /users/notification

##### Parameters
|     Name         |    Type    |    Description    |
| ---------------- | ---------- | ----------------- |
| userToken         |   string   |  No description   |
| deviceID      |   string   |  No description   |
| notification     | tinyint | 1/0 |

##### Response
```json
{
    "result": {
        "status": true
    }
}
```

---

### Send Message

POST /users/sendMessage

#### Parameters
|     Name         |    Type    |    Description    |
| ---------------- | ---------- | ----------------- |
| userToken        |   string   |  No description   |
| message          |   string   |  No description   |
| type             |   string   |  call/feedback/reply/contact   |
| notificationID   |   string   |  Add notificationID for reply and contact type |

##### Response
```json
{
    "result": {
        "status": true
    }
}
```

---

### Get Notification

GET `/users/notification?userToken=0B04B8CE09461A88C1032DA212F6BDE8`

##### Response
```json
200 OK
{
    "result": {
        "status": true,
        "data": [
            {
                "NotificationID": "1C4109D6-82E8-4EE0-9BE7-54E64CB07194",
                "UserID": "0B04B8CE09461A88C1032DA212F6BDE8",
                "Title": "Test",
                "Message": "Notification",
                "Date": "2017-11-19 18:07:19.000",
                "isAccepted": 0,
                "isContacted": 0
            }
        ]
    }
}
```

---

### Remove Notification

POST /users/removeNotification

#### Parameters
|     Name         |    Type    |    Description    |
| ---------------- | ---------- | ----------------- |
| NotificationID        |   string   |  No description   |

##### Response
```json
{
    "result": {
        "status": true
    }
}
```

---

### Get Number of Unread Notification

GET `/users/unreadNotification?userToken=0B04B8CE09461A88C1032DA212F6BDE8`

##### Response
```json
200 OK
{
    "result": {
        "status": true,
        "UnreadNotif": 2
    }
}
```

---

### Accept Notification

POST /users/acceptNotification

#### Parameters
|     Name         |    Type    |    Description    |
| ---------------- | ---------- | ----------------- |
| NotificationID        |   string   |  No description   |

##### Response
```json
{
    "result": {
        "status": true
    }
}
```