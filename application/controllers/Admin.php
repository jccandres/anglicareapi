<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class admin extends CI_Controller {

	public function __construct() 
	{
        parent::__construct();
        $this->load->model('admin_model');
        $this->load->model('log_model');
    }

    public function wrapper($body, $data = NULL) 
	{
		if (isset($this->session->userdata['logged_in_beta'])) {
			$this->load->view('partials/header');
			$this->load->view('partials/left-sidebar');
			$this->load->view($body, $data);
			// $this->load->view('partials/right-sidebar');
			$this->load->view('partials/footer');
		}
		else {
			redirect(base_url());
		}
	}

	public function index()
	{
		if (isset($this->session->userdata['logged_in_beta'])) {
			$this->notifications();
		}
		else {
			$this->load->view('login');
		}
	}

	public function login()
	{
		$data['admin'] = $this->admin_model->getAdmin($this->input->post());
		if ($data['admin']) {
			$this->session->userdata['logged_in_beta'] = 1;
			$this->session->userdata['admin_id'] = $data['admin']->AdminID;
			$this->session->userdata['username'] = $data['admin']->UserName;
			$this->session->userdata['email'] = $data['admin']->Email;
			$this->session->userdata['fname'] = $data['admin']->FirstName;
			$this->session->userdata['lname'] = $data['admin']->LastName;
			$this->session->userdata['admin_level'] = $data['admin']->AdminLevel;
			$this->session->userdata['created_at'] = $data['admin']->CreatedAt;
			//test 1
			//test 2
			redirect(base_url());
		}
		else {
			$msg_data = array('alert_msg' => 'Incorrect Username/Password', 'alert_color' => 'red');
			$this->session->set_flashdata($msg_data);
			$this->load->view('login');
		}
	}

	public function logout()
	{
		$this->session->sess_destroy();
		redirect(base_url());
	}

/* Admin */
	public function admin()
	{
		$data['admins'] = $this->admin_model->getAdmins();
		$this->wrapper('admin', $data);
	}

	public function addAdmin()
	{
		if ($this->admin_model->addAdmin($this->input->post())) {
			$msg_data = array('alert_msg' => 'Admin Added', 'alert_color' => 'green');
			$this->session->set_flashdata($msg_data);
		}
		else {
			$msg_data = array('alert_msg' => 'Failed to add admin', 'alert_color' => 'red');
			$this->session->set_flashdata($msg_data);
		}
		redirect('admin/admin');
	}

	public function changePassword()
	{
		if ($this->admin_model->changeAdminPassword($this->input->post())) {
			$msg_data = array('alert_msg' => 'Password changed', 'alert_color' => 'green');
		}
		else {
			$msg_data = array('alert_msg' => 'Failed to change password', 'alert_color' => 'red');
		}
		$this->session->set_flashdata($msg_data);
		redirect('admin/admin');
	}

	public function deleteAdmin($id)
	{
		if ($this->admin_model->deleteAdmin($id)) {
			$msg_data = array('alert_msg' => 'Successfully deleted', 'alert_color' => 'green');
			$this->session->set_flashdata($msg_data);
			redirect('admin/admin');
		}
	}

	public function updateAdmin()
	{
		if ($this->admin_model->updateAdmin($this->input->post())) {
			$msg_data = array('alert_msg' => 'Update Success', 'alert_color' => 'green');
			$this->session->set_flashdata($msg_data);
		}
		else {
			$msg_data = array('alert_msg' => 'Update Fail', 'alert_color' => 'red');
			$this->session->set_flashdata($msg_data);
		}
		redirect('admin/admin');
	}
/* Admin */

/* Notifications */
	public function notifications()
	{
		$data['users'] = $this->admin_model->getUsers();
		$this->wrapper('notifications', $data);
	}

	public function sendNotification()
	{
		require_once(APPPATH.'libraries/firebase.php');

		$firebase = new Firebase();

		$push_type = "topic";
		$title = $_POST['title'];
		$message = $_POST['message'];
		
		/* Save notification to database */
		$this->admin_model->saveNotif($_POST, $_SESSION['admin_id']);

		/* Get all firebase key of this user */
		$firebaseKey = $this->admin_model->getFireBaseKey($_POST['user']);
		
		// $firebaseKey = "f18L8Zltj00:APA91bFe0ZoC0vfKzWZi7cLexAOtSMUGxtEqZRO5UuDxAWcrnNqO2eqI_bS6D41W_EV0LONX2ACpPb86cXuK5EZmnxtYVIquNzSylcVAIC4U71PxyS3pOeNXjx0kFD3HQQnbCsYqq-6v";
		$notification['title'] = $title;
        $notification['body'] = $message;
        $this->load->model('User_model', 'user_model');
		$notification['badge'] = $this->user_model->getNumberOfUnreadNotif($_POST['user']);
		/* Loop send notif to all firebase key */
		foreach ($firebaseKey as $value) {
			$firebase->sendMultiple($value->firebaseKey, $notification);
		}
		$this->session->set_flashdata($msg_data);
		redirect('admin/notifications');

	}
/* Notifications END*/

/* Logs */
	public function logs()
	{
		$data['logs'] = $this->log_model->get_all();
		$this->wrapper('logs', $data);
	}
/* Logs END */

}