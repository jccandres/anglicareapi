<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class logs extends CI_Controller {

	public function __construct() 
	{
		parent::__construct();
		$this->load->model('admin_model');
		$this->load->model('log_model');
	}

	public function wrapper($body, $data = NULL) 
	{
		if (isset($this->session->userdata['logged_in_beta'])) {
			$this->load->view('partials/header');
			$this->load->view('partials/left-sidebar');
			$this->load->view($body, $data);
			// $this->load->view('partials/right-sidebar');
			$this->load->view('partials/footer');
		}
		else {
			redirect(base_url());
		}
	}

}