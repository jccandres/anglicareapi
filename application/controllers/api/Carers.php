<?php

defined('BASEPATH') OR exit('No direct script access allowed');

// This can be removed if you use __autoload() in config.php OR use Modular Extensions
/** @noinspection PhpIncludeInspection */
require APPPATH . '/libraries/REST_Controller.php';

// use namespace
use Restserver\Libraries\REST_Controller;

class Carers extends REST_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->model('carer_model');
    }

    /*
     * Get List of Carer
     * add '?limit={int}' to get n number of carers
     * default = 50
     */
    public function index_get()
    {
        $data['result'] = new \stdClass();
        // $limit = @$this->input->get('limit');
        $carerToken = @$this->input->get('carerToken');
        $data['result']->data = $this->carer_model->getCarer($carerToken);
        $this->response($data);
    }
}
