<?php

defined('BASEPATH') OR exit('No direct script access allowed');

// This can be removed if you use __autoload() in config.php OR use Modular Extensions
/** @noinspection PhpIncludeInspection */
require APPPATH . '/libraries/REST_Controller.php';

// use namespace
use Restserver\Libraries\REST_Controller;

class Clients extends REST_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->model('client_model');
    }

    /*
     * Get List of Client
     * add '?limit={int}' to get n number of carers
     * default = 50
     */
    public function index_get()
    {
        $data['result'] = new \stdClass();
        $limit = @$this->input->get('limit');
        $data['result']->data = $this->client_model->getClients($limit);
        $this->response($data);
    }

    /*
     * Get Client using clientToken
     */
    public function client_get()
    {
        $data['result'] = new \stdClass();
        $clientToken = $this->input->get('clientToken');
        $userToken = $this->input->get('userToken');
        $data['result']->status = true;
        $data['result']->data = $this->client_model->getClientInfo($clientToken, $userToken);
        $this->response($data);
    }
}
