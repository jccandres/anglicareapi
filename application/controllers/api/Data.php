<?php

defined('BASEPATH') OR exit('No direct script access allowed');

// This can be removed if you use __autoload() in config.php OR use Modular Extensions
/** @noinspection PhpIncludeInspection */
require APPPATH . '/libraries/REST_Controller.php';

// use namespace
use Restserver\Libraries\REST_Controller;

class Data extends REST_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->model('data_model');
    }

    public function index_get()
    {
        
    }

    public function executeQuery_post()
    {
        $data['result'] = new \stdClass();
        $post = $this->input->post();
        $result = $this->data_model->executeQuery($post);
        if ($result) {
            $data['result']->data = $result;
            $data['result']->Status = true;
        }
        else {
            $data['result']->Status = false;
        }
        $this->response($data);
    }

    public function users_get()
    {
        $data['result'] = new \stdClass();
        $data['result']->data = $this->data_model->getUsers();
        $this->response($data);
    }

    public function devices_get()
    {
        $data['result'] = new \stdClass();
        $data['result']->data = $this->data_model->getDevices();
        $this->response($data);
    }

    public function usersDevices_get()
    {
        $data['result'] = new \stdClass();
        $data['result']->data = $this->data_model->getUsersDevices();
        $this->response($data);
    }

    public function schedule_get()
    {
        $data['result'] = new \stdClass();
        $data['result']->data = $this->data_model->getSchedule();
        $this->response($data);
    }

    public function changeVisit_get()
    {
        $data['result'] = new \stdClass();
        $data['result']->data = $this->data_model->getChangeVisit();
        $this->response($data);
    }

    public function cancelVisit_get()
    {
        $data['result'] = new \stdClass();
        $data['result']->data = $this->data_model->getCancelVisit();
        $this->response($data);
    }

    public function requestVisit_get()
    {
        $data['result'] = new \stdClass();
        $data['result']->data = $this->data_model->getRequestVisit();
        $this->response($data);
    }

    public function requestTransport_get()
    {
        $data['result'] = new \stdClass();
        $data['result']->data = $this->data_model->getRequestTransport();
        $this->response($data);
    }

    public function messages_get()
    {
        $data['result'] = new \stdClass();
        $data['result']->data = $this->data_model->getMessages();
        $this->response($data);
    }

    public function notification_get()
    {
        $data['result'] = new \stdClass();
        $data['result']->data = $this->data_model->getNotification();
        $this->response($data);
    }

    public function logs_get()
    {
        $data['result'] = new \stdClass();
        $data['result']->data = $this->data_model->getLogs();
        $this->response($data);
    }

    public function user_by_token_get()
    {
        $userToken = $this->input->get('userToken');
        $data['result'] = new \stdClass();
        $data['result']->data = $this->data_model->getUserByToken($userToken);
        $this->response($data);
    }

    public function get_booking_type_get()
    {
        $clientToken = $this->input->get('clientToken');
        $data['result'] = new \stdClass();
        $data['result']->data = $this->data_model->getBookingType($clientToken);
        $this->response($data);
    }

    public function test_this_get()
    {
        $data['result'] = new \stdClass();
        $data['result']->data = $this->data_model->testThis();
        $this->response($data);
    }

}