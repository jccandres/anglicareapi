<?php

defined('BASEPATH') OR exit('No direct script access allowed');

// This can be removed if you use __autoload() in config.php OR use Modular Extensions
/** @noinspection PhpIncludeInspection */
require APPPATH . '/libraries/REST_Controller.php';

// use namespace
use Restserver\Libraries\REST_Controller;

class Request extends REST_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->model('request_model');

        // $this->load->library('email');
        // $config_mail['protocol']='smtp';
        // $config_mail['smtp_host']='mail.smtp2go.com';
        // $config_mail['smtp_port']='80';
        // $config_mail['smtp_timeout']='30';
        // $config_mail['smtp_user']='betamail@optimindsolutions.com';
        // $config_mail['smtp_pass']='MDFldDJ5a3lkbWk3';
        // $config_mail['charset']='utf-8';
        // $config_mail['newline']="\r\n";
        // $config_mail['wordwrap'] = TRUE;
        // $config_mail['mailtype'] = 'html';
        // $this->email->initialize($config_mail);
    }

    /**
     * Request Visit
     */
    public function visit_post()
    {
        $data['result'] = new \stdClass();
        $post = $this->input->post();
        if ($this->request_model->requestVisit($post)) {
            $data['result']->status = true;
            $this->response($data);
        }
        else {
            $data['result']->status = false;
            $this->response($data);
        }
    }

    /**
     * Request Transport
     */
    public function transport_post()
    {
        $data['result'] = new \stdClass();
        $post = $this->input->post();
        if ($this->request_model->requestTransport($post)) {
            $data['result']->status = true;
            $this->response($data);
        }
        else {
            $data['result']->status = false;
            $this->response($data);
        }
    }

    /**
     * Get list of requests
     */
    public function list_post()
    {
        $data['result'] = new \stdClass();
        $post = $this->input->post();
        $request = $this->request_model->getRequestList($post);
        if ($request) {
            $data['result']->status = true;
            $data['result']->data = $request;
            $this->response($data);
        }
        else {
            $data['result']->status = false;
            $this->response($data);
        }
    }
}