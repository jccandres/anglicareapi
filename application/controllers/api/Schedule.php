<?php

defined('BASEPATH') OR exit('No direct script access allowed');

// This can be removed if you use __autoload() in config.php OR use Modular Extensions
/** @noinspection PhpIncludeInspection */
require APPPATH . '/libraries/REST_Controller.php';

// use namespace
use Restserver\Libraries\REST_Controller;

class Schedule extends REST_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->model('schedule_model');
    }

    /**
     * Get list of schedule
     */
    public function schedule_get()
    {
        $data['result'] = new \stdClass();
        $get = $this->input->get();
        $schedule = $this->schedule_model->getSchedule($get);
        if ($schedule) {
            $data['result']->status = true;
            $data['result']->data = $schedule;
            $this->response($data);
        }else{
            $data['result']->status = false;
            $data['result']->data = "";
            $this->response($data);
        }
    }

    /**
     * (POST) Get list of schedule
     */
    public function schedule_post()
    {
        $data['result'] = new \stdClass();
        $post = $this->input->post();
        $schedule = $this->schedule_model->getSchedule($post);
        // var_dump($schedule);
        // echo "before if";
        if ($schedule) {
            // echo "inside if";
            $data['result']->status = true;
            $data['result']->data = $schedule;
            header("Content-type:application/json");
            echo json_encode($data);
            // $this->response($data);
            // var_dump($data);
        }
        else {
            //echo "else";
            $data['result']->status = false;
            $data['result']->data = "";
            $this->response($data);
        }
        // echo "out"; die();
    }

    /**
     * Change Visit
     */
    public function change_post()
    {
        $data['result'] = new \stdClass();
        $post = $this->input->post();
        if ($this->schedule_model->changeVisit($post)) {
            $data['result']->status = true;
            $this->response($data);
        }
        else {
            $data['result']->status = false;
            $this->response($data);
        }
    }

    /**
     * Cancel Visit
     */
    public function cancel_post()
    {
        $data['result'] = new \stdClass();
        $post = $this->input->post();
        if ($this->schedule_model->cancelVisit($post)) {
            $data['result']->status = true;
            $this->response($data);
        }
        else {
            $data['result']->status = false;
            $this->response($data);
        }
    }

    /**
     * Request Service
     */
    public function service_post()
    {
        $data['result'] = new \stdClass();
        $data['result']->status = false;
        $this->response($data);
    }

    /**
     * Request Transport
     */
    public function transport_post()
    {

    }

}
