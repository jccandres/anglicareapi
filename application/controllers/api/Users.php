<?php

defined('BASEPATH') OR exit('No direct script access allowed');

// This can be removed if you use __autoload() in config.php OR use Modular Extensions
/** @noinspection PhpIncludeInspection */
require APPPATH . '/libraries/REST_Controller.php';

// use namespace
use Restserver\Libraries\REST_Controller;

class Users extends REST_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->model('user_model');

        $this->load->library('email');
        $config_mail['protocol']='smtp';
        $config_mail['smtp_host']='mail.smtp2go.com';
        $config_mail['smtp_port']='80';
        $config_mail['smtp_timeout']='30';
        $config_mail['smtp_user']='anglicare';
        $config_mail['smtp_pass']='Ymo4cnE4N3ZsNWcw';
        $config_mail['charset']='utf-8';
        $config_mail['newline']="\r\n";
        $config_mail['wordwrap'] = TRUE;
        $config_mail['mailtype'] = 'html';
        $this->email->initialize($config_mail);

    }

    public function index_get()
    {
        
    }

    /**
     * Register User
     */
    public function RegisterUser_post()
    {
        $data['result'] = new \stdClass();
        $post = $this->input->post();
        $user_data = $this->user_model->getUser($post);

        // $client_ids = array('7F734D01-41A6-4BE0-BA67-2F4EED3F6835','3D79F7D5-DECC-4C55-9512-20EE2654512B','D4A2199E-27D8-47DA-95B8-57F96DDF876B','2247FC71-C5BE-48D4-948A-159157A2624E','EBF43844-8E6A-4BF8-A964-64E4B63B4737','5DACBA28-B72C-484F-9084-89EFEE76121C','4CBDE309-0BEA-48BF-87F8-0114F6B9ACA4','9BF4FC0F-7B46-4226-833A-32BA909D7046','02A20619-6D47-4D81-A72C-64B6B1BEFCDF','FE9EB8AB-77A6-44F3-8C82-4458C8AAC00B', 'D76D93BB-28ED-4463-8EC6-328DB25EAF68','7D9AA7FE-E495-4CB3-A515-1801D02C1F86','187D3B4F-9862-493D-BF1E-1117A93A98FE','C6BE8D3D-B443-4C73-A8AD-426B7FF8296D','E5C60971-5219-4ADA-8270-BE03516CC87E','EB5E59CA-5F60-4376-9C85-9DB81D5EB4CB','809B622C-8131-4DA1-96F2-0CCD2D86C18C','EEF891F8-8919-4CD6-96FE-EEEF68CDB6A2','285243B0-8ED5-46D7-B5CA-214F8EA1DC53','B3102915-E50C-47C0-8188-EFA5D8F7A68D','EF18A11A-080C-4A26-A7E6-5EC9626358FE','49698E64-F02D-42B9-8CD4-121160D803A6','A6C2567D-17A1-4843-992B-3659CEED33AE','8530F2EE-F879-4F97-89ED-7C67859882D0','44748742-FD26-4007-913C-C72FC312A3E8','BCF540EC-DEB1-43F9-A72A-8C1F64342298','BC65B0F2-9E18-4BD4-A4A9-49BAD84B8149','6535F4D3-34E1-4C4F-81B4-5DB272463C89','81830C57-FE5F-47C9-B18C-ECD4CAC8469D','9F1C7136-B38E-42E1-928E-B33C98D1909A','2C300BEF-8597-435E-A956-20A98F3E5D14','001FF92D-BF9D-497E-ADA4-D4AB9AB58EE6','76F65426-0575-45AC-9B6A-4B3EDF9F20DF','9049A111-55D8-4810-91CE-53B23A5F6AB0','8AC309BB-D86F-44CA-9D2B-87349EB48792','FDFFDDE3-5BA5-462A-A643-B89DB3EBDBD4','2A69B4D1-5170-429D-A0C2-CD6F87ABD386','C409EDBD-DA47-4CB7-AAC5-0F526AB5ADD4','B2888743-7BFA-40DE-BC7D-422F53A591A4','E775CDC6-4FE2-4EF4-B465-2ABE5D70755D','748F950F-2E32-49D7-B6A5-073D4AC8627C','D84F5961-A4AB-4F65-BC86-85370CF66DD8','848244ED-5CAB-44FF-8787-D228F19BD9AC','6CC11CB8-2427-49D4-9185-EE834345ACBE','AAD611BE-D27F-4EBF-BCFD-971C887F4ECE','6DC6F745-DD22-4E6E-84F8-D8F5C3428966','30198431-A633-4342-9875-6773AF189122','8D9D56C4-2AC0-404C-BD75-6DC59CD87343','D79E894E-0AE5-46A2-9514-F91B8D04A900','49BA4A8A-5429-48B9-AD4A-2205DB9875BE','306987C6-3400-46E9-A194-25CADBD033B1','139BD8B9-F1BF-4964-82ED-54004CB28BE4','C8C672EB-239D-4A1C-9A85-07947935100C','CC5F2A55-F8E6-4924-975C-12A5622CFADD','D1255E38-C091-4331-AAF8-ABAEFD5431F5','676CA63E-2B42-43C6-8767-D0C36417960B','B1592E9B-542F-48E5-9DA2-B743976407AC','14D41F54-FA4B-4DD7-8EEE-4970F7C9BAE1','F0A6A0D4-6718-43EB-B529-D2194790C4D4','B93B1FB3-1834-4396-84C6-847554235AB7','158CDC39-F627-42E0-B8F8-70F19A0CD2B6','CF404BEA-AE0A-461F-92D3-8599E61B37E0','B876843D-C52A-4756-9398-3D9844AAA311','EA96C3EF-EF9F-4013-B0C0-BBB5A10BE737','FB6FBC86-322C-4840-A560-BD1F406A980B','66C4E801-AF5A-4E3F-BCA1-DE8EA283FB36','CFE58308-A56E-4694-92FC-273A0BE6DD3D','78B13E6C-89D6-4BEC-A518-9FE1B5CBBAAE','ACE86F09-44AA-4787-B741-EA1C6C733A82','B925AFD0-C610-4179-95B2-D396FFD5B477','96841A19-64F2-4B45-9A5A-AE5CD77BBC46','73897046-9F01-4EE0-965D-AE8C358966F8','2421C8DE-7694-4E01-B5D7-F8DF3D5DD32B','DACB3D55-98EE-49D6-8F8A-EE7BE2636180','AAFF740A-D597-4573-B576-C485C87FF626','2C825781-7FB6-48A2-9C3C-CFD09AD2998C','31C90BD2-C2C3-42E3-903E-65A7A791DA60','7071DAA9-2A31-49F1-800B-BF9F296998BD','6C5257CC-5B73-487F-B199-8270EA8628E3','52F8C18A-4800-45CB-BBFF-A570EFAB812B','D44F5B0E-12DF-44F0-A2BD-3255F253FA80','5F3BB092-37AE-4DEF-AF22-C940A9FF0268','B5E24ABB-6119-4010-9897-95963F2F51CA','14336E06-1BB7-42EF-B545-BE6C2A325024','7D1F5B67-EA1F-4ED8-93C6-408881E15C58','8924BD83-02C3-47FD-99CA-895F593DEDF4','F8F6686F-9A28-492B-A0F5-F31EF9105D81','AF5A123E-7CD4-403A-9080-61E01D7F5BB1','130B2C59-AE7C-466E-ADFF-1519EF8632F6','42A3E950-B003-407A-9771-1B90C70A87DE','B2B1FEC2-F58A-492E-93C3-63471E81632A','FC04168D-BCB0-44CC-B9A0-7E0503663148','8B5667EA-F51A-4AC8-8E8A-F92356631E6C','D969B8F5-FA61-4A3F-A22B-BA59B5F30D99','D9BE789A-C5B4-4BD6-9E36-D4A02140AEFA','2853C525-3D2E-4589-8345-0289154E3E18','D64C6EAB-3B7B-43BD-9E0E-341EE7C554A6','159B096C-6E1A-4A16-80AB-AD0874AFB10A','BD1F6324-D812-4B41-9538-4153D7656CC2','E19509BD-72D0-4535-B120-BCB3C928E7E3','76FB51F7-7E91-4902-8F8D-2FFB20494F97','FB2E947F-29E8-4A76-9496-3610A3925903','30075D26-E9DF-4E1E-A31F-2D3F965CD55E','CB01B5F9-6168-4318-9ADF-6C7F64E046A2','010BED3D-CC96-4936-AB9D-E9BE5F01DE51','1C736DFB-0018-4D19-AE13-8D426B82341C','7773D52E-1A8C-421B-A533-600E12348CAE','AB788508-B44B-492A-8512-F645F2E44905','514DBB9B-C664-403A-B227-9353952195D4','AE5AE2B9-DA74-456A-9D11-DB3344421B93','06E9F15C-068C-40CB-A3DA-9458D6732EAE','77881065-0348-4D6D-AEED-8EDED284D850','40D181D4-A942-4600-B968-E48831DB520E','0538F18A-CB56-457E-8B1D-A4204606962D','0DF9E0EC-4688-4263-96CC-9F2C3F653469','A7D04F43-523C-461C-A3FB-E03354725944','545C4BBF-C790-44A3-8D8D-BB6000EF3534','F1B2EE54-F655-4C4D-8F53-15FA1C8E8756','84AE935C-930C-440B-BF0E-774693ECF2DA','934FF744-E707-4F17-94FD-F447FA689C96','0919BA8B-112F-4E18-AE97-3A961979CCFF','5921820F-7CA3-4CD7-8DF3-CB607FA2071A','67667E69-ED41-4816-981F-0CCC59C0561E','FB6A0B97-14B5-4F85-AA3E-3D93FF92DB84','C91C753C-B9BE-4165-A224-26DC0A78117A','6CE37D32-A6DB-4CC1-BBAD-E50201157809','16412F08-1F09-4350-AC37-743E0D14AA8B','953836ED-18C0-4C3B-8C25-E087405A1580','24ABD338-DDAE-43A2-B3C4-78E849289BF7','FF6F7894-4FF3-4DC9-B7D2-583756FCCBE5','36FB7F82-B540-4C9B-A03A-9EBADBF5D41E','5E18ABCD-85FF-49CD-9BB5-C3A8F349DF3F','185DE600-D2F6-4528-85E1-565D1612AE93','83178DAD-6180-45F1-BD3D-3DF92365766D','E7D25C7F-5F30-4AD2-ABC2-C58C6AAB2E00','C4D2495A-DC80-4DF1-99B3-D8B08E720028','42003EB2-52B1-4D59-9581-EC27FFAF3510','D96B52F5-8F51-4F0B-91EF-E62D82C0A048','5335C4FE-2712-4341-A074-03EBF26D626E','9C6145D2-BD2E-47E0-AA7B-E00578045863','84380074-A1C5-443A-B230-529907D5C3D6','C4C453AD-520C-4763-82FF-97F9A330BBD0','ED6199BE-B423-468A-A5A1-90B3BEDDD42F','98D8470A-214D-4C23-B7BE-6C4211E33EC5','7A94C28E-C827-4B44-B0B4-20D09D448DBC','6849588F-726A-4793-A4D8-D5484B663909','3EA9193A-AF07-4A6A-965B-6FBBDC499C27','34ECAE5A-4BA2-4481-B270-FEF12A072570','B3B2332C-721B-4573-8AD2-7C3D8B7F08AA','EEB9E92E-6B36-4336-8A9A-5A648BE149D8','84AF1F3E-DFA0-4B43-AD13-F5B7DF839869','CA99A5FA-91D2-4307-8690-7D34D58B184E','7C1BE4C6-57FB-4786-955B-797F16147E1B','F811C828-8D36-4F2F-A649-503C263CB78E','315E1DBB-8341-44AE-BB7E-4CF820367C7B','02193BC4-3027-4AAD-AD60-41B265052B73','3E537BE3-7CEF-4DE9-8A19-6AD8C2582940','71771210-1B58-402D-9581-C90E5D4E8061','C97AA376-15C2-421A-BDD8-E2AEC6382E51','F811C828-8D36-4F2F-A649-503C263CB78E','48D91561-655F-4F2A-B2A2-C468723C1DCF','B00D5274-C8ED-49F9-9848-CBD4F5F135DA','E33E1A2C-4F66-4114-A7C9-C6516D7A7F1E','6DE313CC-BEF3-457F-A107-8DB3E2055C18','00CD3E4B-7E8F-45ED-AB2B-66AFECFED693','00CD3E4B-7E8F-45ED-AB2B-66AFECFED693','A05157EB-D544-4E43-8C8B-3F7BF80E71FC','6DED01D0-0403-43E4-8CC0-3D69A8104E38','0B03C9A9-8379-4CF7-BACB-641F8CEC3B56','77881065-0348-4D6D-AEED-8EDED284D850','7153DD36-5D00-43B9-91E2-CE289C8AB91E','180DA94F-9E6B-4935-A058-A48588CAED58','5E5E0352-82E3-48CC-907D-E210A04C73BD','1451FCB3-CE1B-4C55-AF1B-904771035529','7153DD36-5D00-43B9-91E2-CE289C8AB91E','AFEE35BB-48AB-4780-9964-2E661C3125D8','5471BCB4-AB2D-42B0-8D8F-8397D60123C3','7153DD36-5D00-43B9-91E2-CE289C8AB91E','EE70DC03-6B23-4B91-8419-19D88483D38D','2734786B-1E14-40DE-B9A7-6395F799AF34','26A9A36B-E7FD-4770-B857-2041B7A2CC1D','4E18E0D4-4332-48B1-97CD-3814F724638B','9403B560-98D8-4DA8-A385-2952FEEE462F','908FC154-C3EA-4056-AF02-ACAAF3047C66','F2110CD9-EBFE-4890-A928-CD77284F8485','18E2FF7E-EFCB-4924-B14C-64EA3DB427AC','E813B4D6-55B3-4DFA-ADA0-5F9F2DA0082B','58F3EA9B-C3C1-4B78-927E-0CA283172E7E','5F9A5A35-DAA9-459D-891E-296060B2BDED','85E36DC8-2644-4870-8AD9-18D6D8AF8DC3','F6FED85E-01CF-4639-A726-3138E617B387','0FDE60AD-5AE9-46EE-8CED-6E61064ABB13','999C251C-8E4A-4053-A9EA-F95160FAC2A1','4B127CE7-DB55-4470-9C5D-D8E34FAAB548','D0F1AD73-05B2-466F-8C25-EEEE4B4EF980','5B2C4309-4130-4F2E-91AC-D90DF1EAAF3F','DFC81118-103D-44E3-8DCB-D3995D9FE011','B9EFD5AF-461C-4599-B9D0-9040393A616E','8B9943F0-D0C7-492D-9527-ACDBE7DE24A3','30363052-15C8-4302-96AF-14F017B775C1','3288ACC2-DDF5-41DB-900E-0EF376E6184F','5C942098-C3C4-4C44-B0AA-D78DDAEB942D','6ABD0CF5-B311-42D3-AA46-42DAEACC0B1F','5D82CCBA-B25E-44B6-B3FF-CCE424C97E6D','296DF719-55E4-489D-AF65-04D3D4EC8737','C29CF406-FC92-41F2-83EE-07A09DF1CCB6','9A4E7D99-64A2-4ED6-8A73-EF34FBAAAC4E','AC592405-9AD5-4C06-93A8-66FA5D8B3416','BAEEDB4B-BBA1-4804-B812-C9E0C0CBE42C','C23825DE-BAB3-41E1-8A74-548A557C8730','1BEE2DB9-1CA5-4F8E-8A56-A54391805650','0D2773B9-A32A-4865-9134-5F8F254AD58F','E3B07725-5999-4ABB-A58F-4C4AAC430623','FD4C2583-6B4F-4C11-9609-3D1415AD497E','23550E1B-8EE0-4256-9DF9-16187A1D432A','FB24863E-EA0B-4506-A74B-F735FA18E30A','8708D353-60F6-4596-9098-16D229AA3713','173278E4-CC49-48E8-AC61-7A7F159E19DB','DEA4FFA4-7A2B-4F96-A8A4-DBA865CBC1D5','A549E2E3-B101-473B-A3FF-EC7F0FAA164B','96B7B505-49D0-47B3-929A-E57A4FA7672F','D843E9F8-AA73-4AA0-B364-DA325C59479A','D9BA27F2-84D1-4038-999A-DC1A9E82A265','24208BF0-2EDD-4B98-B677-1043F5832A0D','5AC13A5B-5C19-4EB3-8E4D-FAACC75AA316','299B5937-0238-43EE-A3B8-7680E2548669','884C9D35-54D5-4BE7-9841-9B8F372D6E29','481A517F-B3A5-4F0A-878E-B93DEB8159B8','00747B09-AD86-4DFA-8334-0B571DDAB982','D8E8E984-8977-4EEE-87D0-F193413FEF02','6410D4F1-0765-446B-B17D-0E49F7A373FE','D5EDC2AA-76E7-4B00-BB47-A06F5C1575D9','8D88084A-699B-4DB9-960B-3C0BE0C03CDB','D9648805-7726-4723-97ED-A54F613FEF99','F10367FA-CA30-42C4-903E-829D42C846A0','66648454-E298-455C-98DD-E93C56E2C54A','2AA7AA7A-FA7F-441C-82A5-1C5B4B29DC41','4D753C91-5DCC-46A1-833F-8107A11A00A0','ABC55343-4CE2-4A4E-A66B-C7E0E32017F5','6010AD58-5724-4212-981A-FD7D355EA74C','C093E7E1-E9C2-4BB9-AD96-C7D5D37BE20E','D8DB8740-4BCD-44A3-B95C-C1A16A1D0E67','6A2FEFDB-FFAF-415F-85F3-2567A9191077','8C52C3D6-B511-41AA-9AF0-4441451A3D19','53737AD1-E830-4EFF-8116-763BFFE5CCCE','A4DC7836-F36F-4456-8127-0322E0837964','EE41D672-023B-48A3-A980-0D8664D08F0E','D104ADE1-B34F-46F2-BEC6-37814737CA25','5A3ED931-8B67-4D17-BEA8-CDEFFD5D7A94','E7762EFE-3506-484A-9769-A6EEB253764A','CAB82687-D493-4D37-8800-89D82DE6E726','B03659C8-270B-4EA1-A93B-C72764D971AB','F632A1D2-D96F-423B-B97D-5995F656ACA3','76DFECD9-4FCE-4831-8B82-C25D94AB8035','EE1869DE-05E9-4BFE-9627-D7B3F2C9FD84','95FD6E17-C38E-4C3C-904D-DE48CD95257B','4CA0E1AC-ACE2-4968-BDDD-C3E19829EA89','807F565C-C841-4BB8-B922-45F088A3C434','D7D98063-4431-4C6B-9480-A918FE4324A5','211EEAC2-9048-4905-97AE-B61445697F85','8608945F-B1B8-447E-A004-F0EFE5222851','A39F268D-A036-47EA-9C93-86E4401269AE','0F0182D0-8A0E-4048-AA22-F8081E747701','91355319-7C4C-4942-8407-91A912244585','DFD7ABD6-24DC-444C-B676-A40731CF797D','1B4AEA54-0508-455A-84AF-682ED751BF23','CFDD8AF8-E568-404F-83CF-2223736E16B2','A82CD3BC-EAFD-43B1-8B62-88420673200D','A851F6B9-0C92-4FDE-9CEF-9E8E4F1C12AA','FF2816B4-AE52-4993-8CC8-18F05D1165C2','B0DA9E06-98B4-4D1E-B006-25EF7F93A51A','8EBA6806-36C8-4DA5-BB0A-3BED2D1DF9DC','41AE680F-4099-42EE-8AB6-18E4BA68DD3F','D3670C94-0D81-4F60-88F0-84F31B6D3FE2','8C1D9444-1AE2-48F3-9660-F7138D023CD6','AA0B0D11-69E4-4445-8897-757BE557FBB2','918F7697-35F0-4AC9-A5FA-FBA75B8FA380','CF51B156-57E7-49FE-AE0C-C6D884532EBF');
        
        if ($user_data['status']) {
            unset($user_data['status']);
            if (true){ # in_array($user_data['clientToken'], $client_ids)
                $data['result']->status = true;
                $data['result']->data = $user_data;
                $this->response($data);
            }else{
                $data['result']->status = false;
                $data['result']->data = $user_data;
                $this->response($data, 404);
            }
        }
        else {
            unset($user_data['status']);
            $data['result']->status = false;
            $data['result']->data = $user_data;
            
            $this->response($data, 404);
        }
    }

    /**
     * Send SMS Verification Code
     */
    public function sendSMSVerificationCode_post()
    {
        $data['result'] = new \stdClass();
        $post = $this->input->post();
        if ($this->user_model->sendSMSVerificationCode($post['userToken'])) {
            $data['result']->status = true;
            $data['result']->data = $this->user_model->getUserByUserId($post['userToken']);
        }
        else {
            $data['result']->status = false;
            $data['result']->data = [];
        }
        $this->response($data);
    }

    /**
     * Verify Code
     */
    public function verifyCode_post()
    {
        $data['result'] = new \stdClass();
        $post = $this->input->post();
        if ($this->user_model->verifyCode($post['userToken'], $post['code'])) {
            # delete code from db
            $smsCode = "verified";
            $this->user_model->saveVerificationCode($post['userToken'], $smsCode);

            $data['result']->status = true;
            $data['result']->message = "Success";
        }
        else {
            $data['result']->status = false;
            $data['result']->message = "Invalid Code";
        }
        $this->response($data);
    }

    /**
     * Register PIN
     */
    public function pin_put()
    {
        $data['result'] = new \stdClass();
        $put = $this->put();
        if ($this->user_model->updateUserData($put))
        {
            $data['result']->status = true;

            /* Booking Type */
            if (isset($put['clientToken'])) {
                foreach ($this->user_model->getBookingType($put['clientToken']) as $value) {
                    $data['result']->BookingType[] = $value->BookingType;
                }
                if ($put['clientToken'] == "EBF43844-8E6A-4BF8-A964-64E4B63B4737") {
                    $data['result']->BookingType[] = 'Transport';
                }
            }
            /* Booking Type */

            /* Registered Mobile Number */
            // foreach ($this->user_model->getRegisteredMobileNumber($put['userToken']) as $value) {
            //     $data['result']->RegisteredNumber = $value->RegisteredNumber;
            // }
            $data['result']->RegisteredNumber = $this->user_model->getRegisteredMobileNumber($put['userToken']);
            /* Registered Mobile Number */

            $this->response($data);
        }
        else {
            $data['result']->status = false;
            $data['result']->data = [];
            $this->response($data, 404);
        }
    }

    /**
     * Validate PIN
     */
    public function pin_post()
    {
        $data['result'] = new \stdClass();
        $post = $this->input->post();
        if ($this->user_model->validatePIN($post)) {
            $data['result']->status = true;

            /* Booking Type */
            if (isset($post['clientToken'])) {
                foreach ($this->user_model->getBookingType($post['clientToken']) as $value) {
                    $data['result']->BookingType[] = $value->BookingType;
                }
                if ($post['clientToken'] == "EBF43844-8E6A-4BF8-A964-64E4B63B4737") {
                    $data['result']->BookingType[] = 'Transport';
                }
            }
            /* Booking Type */

            /* Registered Mobile Number */
            // foreach ($this->user_model->getRegisteredMobileNumber($post['userToken']) as $value) {
            //     $data['result']->RegisteredNumber = $value->RegisteredNumber;
            // }
            $data['result']->RegisteredNumber = $this->user_model->getRegisteredMobileNumber($post['userToken']);
            /* Registered Mobile Number */

            $this->response($data);
        }
        else {
            $data['result']->status = false;
            $this->response($data, 404);
        }
    }

    public function test_get()
    {
        var_dump($this->user_model->test()); die();
    }

    /**
     * Update firebase key
     */
    public function firebaseKey_put()
    {
        $data['result'] = new \stdClass();
        $put = $this->put();
        if ($this->user_model->updateFirebaseKey($put)) {
            $data['result']->status = true;
            $this->response($data);
        }
        else {
            $data['result']->status = false;
            $this->response($data, 404);
        }
    }

    /**
     * Unlink user to a device
     */
    public function unlink_put()
    {
        $data['result'] = new \stdClass();
        $put = $this->put();
        if ($this->user_model->unlinkUser($put)) {
            $data['result']->status = true;
            $this->response($data);
        }
        else {
            $data['result']->status = false;
            $this->response($data, 404);
        }
    }

    /*
     * Notification toggle
     */
    public function notification_put()
    {
        $data['result'] = new \stdClass();
        $put = $this->put();
        if ($this->user_model->updateNotification($put)) {
            $data['result']->status = true;
            $this->response($data);
        }
        else {
            $data['result']->status = false;
            $this->response($data, 404);
        }
    }

    /*
     * Get Notifications
     */
    public function notification_get()
    {
        $data['result'] = new \stdClass();
        $userToken = $this->input->get("userToken");
        $data['result']->status = true;
        $data['result']->data = $this->user_model->getNotification($userToken);
        $this->response($data);
    }

    /*
     * Remove Notification
     */
    public function removeNotification_post()
    {
        $data['result'] = new \stdClass();
        $post = $this->input->post();
        if ($this->user_model->removeNotification($post)) {
            $data['result']->status = true;
            $this->response($data);
        }
        else {
            $data['result']->status = false;
            $this->response($data, 404);
        }
    }
    
    /*
     * Accept Notification
     */
    public function acceptNotification_post()
    {
        $data['result'] = new \stdClass();
        $post = $this->input->post();
        if ($this->user_model->acceptNotification($post)) {
            $data['result']->status = true;
            $this->response($data);
        }
        else {
            $data['result']->status = false;
            $this->response($data, 404);
        }
    }
    
    /*
     * Get Total Notification that the user did not reply to or contact 
     */
    public function unreadNotification_get()
    {
        $data['result'] = new \stdClass();
        $userToken = $this->input->get("userToken");
        $data['result']->status = true;
        $data['result']->UnreadNotif = $this->user_model->getNumberOfUnreadNotif($userToken);
        $this->response($data);
    }

    /*
     * Messages
     */
    public function sendMessage_post()
    {
        $data['result'] = new \stdClass();
        $post = $this->input->post();
        // var_dump($post); die();
        if ($this->user_model->sendMessage($post)) {
            #Send Email here ..
            // $this->email->from('noreply@anglicare.com', 'Anglicare');
            // $this->email->to('jcandres@myoptimind.com');
            // $this->email->subject('Test');
            // $msg = "
            // Test
            // ";
            // $this->email->message("Test Message");
            // $this->email->send();

            $data['result']->status = true;
            $this->response($data);
        }
        else {
            $data['result']->status = false;
            $this->response($data, 404);
        }
    }
    
    /*
     * Find user by first name and last name
     */
     public function findUser_get()
     {
         $data['result'] = new \stdClass();
         $firstName = $this->input->get('firstName');
         $lastName = $this->input->get('lastName');
         
         $data['result']->contact = $this->user_model->getContactData($firstName, $lastName);
         $data['result']->client = $this->user_model->getClientData($firstName, $lastName);
         var_dump($data); die();
         $this->response($data);
     }

     public function dev_test_get()
     {
        $userToken = $this->input->get('userToken');
        $num = $this->user_model->getRegisteredMobileNumber($userToken);
        var_dump($num); die();
     }

     public function sendSMSCurl_post()
     {
        $post = $this->input->post();
        // $this->email->from('noreply@anglicare.com', 'Anglicare');
        // $this->email->to('jcandres@myoptimind.com');
        // $this->email->subject('Test');
        // $this->email->message("Mobile: " . $post['mobile_no'] . "<br> Message: " . $post['msg']);
        // $this->email->send();
        SendMessage("CI00092511", "aah.clientapp@anglicare.org.au", "123456789", $post['mobile_no'], $post['msg']);
        $this->response($post);
     }
}
