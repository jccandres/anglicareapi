<?php

#random string generator
function generateRandomString($length = 32)
{
    $characters = '0123456789ABCDEF';
    $charactersLength = strlen($characters);
    $randomString = '';
    for ($i = 0; $i < $length; $i++) {
        $randomString .= $characters[rand(0, $charactersLength - 1)];
    }
    return $randomString;
}


/**
 * Red Oxygen SMS
 */
function SendMessage($AccountID, $Email, $Password, $Recipient, $Message)
{
	$Parameters['AccountID'] = $AccountID;
	$Parameters['Email'] = $Email;
	$Parameters['Password'] = $Password;
	$Parameters['Recipient'] = $Recipient;
	$Parameters['Message'] = $Message;
 
	Request($Parameters, 'http://www.redoxygen.net/sms.dll?Action=SendSMS');
}

function Request($Parameters, $URL)
{
	$URL = preg_replace('@^http://@i', '', $URL);
	$Host = substr($URL, 0, strpos($URL, '/'));
	$URI = strstr($URL, '/');
	$Body = '';
 
	foreach($Parameters as $Key => $Value)
	{
		if (!empty($Body))
		{
			$Body .= '&';
		}
 
		$Body .= $Key . '=' . urlencode($Value);
	}
 
	$ContentLength = strlen($Body);
 
	$Header = "POST $URI HTTP/1.1\n";
	$Header .= "Host: $Host\n";
	$Header .= "Content-Type: application/x-www-form-urlencoded\n";
	$Header .= "Content-Length: $ContentLength\n\n";
	$Header .= "$Body\n";
 
	$Socket = fsockopen($Host, 80, $ErrorNumber, $ErrorMessage);
	fputs($Socket, $Header);
 
	while (!feof($Socket))
	{
		$Result[] = fgets($Socket, 4096);
	}
	fclose($Socket);
}

if (!function_exists('curl_api')) {
	function curl_api($url, $type, $arr_data = NULL)
	{
		// $url = str_replace('api2', 'api', $url);
		$curl_handle = curl_init();
	    curl_setopt($curl_handle, CURLOPT_URL, 'https://api.arv.org.au/AnglicareAPI/api/' . $url);
	    // curl_setopt($curl_handle, CURLOPT_URL, 'https://mybakery-puratos.com/api/dev/mail_test');
	    curl_setopt($curl_handle, CURLOPT_RETURNTRANSFER, true);
	    curl_setopt($curl_handle, CURLOPT_USERAGENT, "curl");
	    curl_setopt($curl_handle, CURLOPT_TIMEOUT, 1);
	    curl_setopt($curl_handle, CURLOPT_CUSTOMREQUEST, strtoupper($type));

	    $headers = array(
	    	'Authorization: Basic YWRtaW46MTIzNA==',
	    	'x-api-key: 12345',
	    	);
	    curl_setopt($curl_handle, CURLOPT_HTTPHEADER, $headers);

	    if ($type == 'put' || $type == 'post' || $type == 'delete') {
	    	curl_setopt($curl_handle, CURLOPT_POSTFIELDS, http_build_query($arr_data));
	    }
	    // if ($type == 'put' || $type == 'delete') {
	    	// curl_setopt($curl_handle, CURLOPT_HTTPHEADER, array('Content-Type: application/x-www-form-urlencoded'));
	    // }
	    
	    curl_setopt($curl_handle, CURLOPT_SSL_VERIFYPEER, false);
	    curl_exec($curl_handle);
	    curl_close($curl_handle);
	         
	    // $result = json_decode($buffer);
	    // return $result;
	}
}