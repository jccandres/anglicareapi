<?php

date_default_timezone_set("Australia/Sydney");

class Admin_model extends CI_model {
/* Admin */
	public function getAdmin($post)
	{
		$query = $this->db->query("SELECT * FROM dbo.admin WHERE UserName = '" . $post['username'] . "'");
		if ($query->num_rows() >= 1) {
			$result = $query->result()[0];
			if (password_verify($post['password'], $result->Password)) {
				return $result;
			}
			else {
				return false;
			}
		}
		else {
			return false;
		}
	}

	public function getAdmins()
	{
		$query = $this->db->query("SELECT * FROM dbo.Admin");
		return $query->result();
	}

	public function addAdmin($post)
	{
		$CreatedAt = date("Y-m-d H:i:s");
		$post['password'] = password_hash($post['password'], PASSWORD_DEFAULT);
		$query = $this->db->query("INSERT INTO dbo.Admin (UserName, Password, Email, FirstName, LastName, AdminLevel, CreatedAt) VALUES ('" . $post['username'] . "', '" . $post['password'] . "', '" . $post['email'] . "', '" . $post['fname'] . "', '" . $post['lname'] . "', '" . $post['admin_level'] . "', '" . $CreatedAt . "')");
		return $query;
	}

	public function changeAdminPassword($post)
	{
		$data['username'] = $post['username'];
		$data['password'] = $post['old_password'];
		if ($this->getAdmin($data)) {
			unset($data);
			$data['password'] = password_hash($post['new_password'], PASSWORD_DEFAULT);
			$data['admin_id'] = $post['admin_id'];
			$query = $this->db->query("UPDATE dbo.Admin SET password = '" . $data['password'] . "' WHERE AdminID = '" . $data['admin_id'] . "'");
			if ($query) {
				return true;
			}
			else {
				return false;
			}
		}
		else {
			return false;
		}
	}

	public function updateAdmin($post)
	{
		$query = $this->db->query("UPDATE dbo.Admin SET 
			UserName = '" . $post['username'] . "',
			Email = '" . $post['email'] . "',
			FirstName = '" . $post['fname'] . "',
			LastName = '" . $post['lname'] . "'
			WHERE AdminID = '" . $post['admin_id'] . "'
			");
		return $query;
	}

	public function deleteAdmin($id)
	{
		$query = $this->db->query("DELETE FROM dbo.Admin WHERE AdminID = '$id'");
		return $query;
	}
/* Admin END */

/* For Notification START */
	public function getUsers()
	{
    	// $arr_data = array_merge($this->getUsersClient(), $this->getUsersCarer());
    	$arr_data = $this->getUsersClient();
    	return $arr_data;
	}

	public function getUsersClient()
	{
		$query = $this->db->query("SELECT Users.userID, HCM_Client.FirstName, HCM_Client.LastName, Users.type FROM dbo.HCM_Client
			INNER JOIN dbo.Users ON HCM_Client.ClientID = Users.clientID ORDER BY HCM_Client.FirstName ASC");
		return $query->result();
	}

	public function getUsersCarer()
	{
		// $query = $this->db->query("SELECT Users.userID, Users.carerID FROM dbo.Users WHERE Users.carerID != ''");
		// return $query->result();
	}

	public function getFireBaseKey($userID)
	{
		$query = $this->db->query("SELECT Devices.firebaseKey FROM dbo.Devices INNER JOIN dbo.users_devices ON Devices.deviceID = users_devices.deviceID WHERE users_devices.userID = '$userID' AND users_devices.isactive = 1");
		return $query->result();
	}

	public function saveNotif($data, $admin_id)
	{
		// var_dump($data); var_dump($admin_id); die();
		$CreatedAt = date("Y-m-d H:i:s");
		$query = $this->db->query("INSERT INTO dbo.Notification (UserID, Title, Message, CreatedAt, AdminID, IsDeleted) 
			VALUES ('" . $data['user'] . "', '" . $data['title'] . "', '" . $data['message'] . "', '" . $CreatedAt . "', '" . $admin_id . "', 0)");
		return $query;
	}
/* For Notification END */
}