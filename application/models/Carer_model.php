<?php

class Carer_model extends CI_model {
    public $db_ccm;

    public function __construct()
    {
        parent::__construct();
        // $this->db_ccm = $this->load->database("CCM_Live", TRUE);
    }

    public function getCarers($top)
    {
        $top = ($top == NULL) ? "50" : $top;
        $query = $this->db->query("SELECT TOP $top FirstName, LastName, Telephone1, Telephone2, Telephone3 FROM dbo.HCM_Contact WHERE ContactType = 1");
        return $query->result();
    }

    /*
     * Get carer info
     * Required Parameters: carerToken
     * Database: dbo.Contact
     */
    public function getCarer($carerToken)
    {
        $query = $this->db->query("SELECT ContactType, CategoryRef, FirstName, LastName, Telephone1, Telephone2, Telephone3 FROM dbo.HCM_Contact WHERE ContactID = '" . $carerToken . "' AND ContactType = 1");
        $result = $query->result();
        foreach ($result as $key => $value) {
            $result[$key]->FirstName = trim($value->FirstName);
            $result[$key]->LastName = trim($value->LastName);
        }
        return $result;
    }

    /**
     * Get primary carer token from dbo.PersonContact
     * @param  [string] $id [PrimaryCarerRef]
     * @return [string] $id [CarerID]
     */
    public function getPrimaryCarerToken($id)
    {
        $query = $this->db->query("SELECT ContactRef FROM dbo.HCM_PersonContact WHERE PersonContactID = '" . $id . "'");
        return $query->result();
    }

    public function getCarersToken($clientToken)
    {
        $query = $this->db->query("SELECT ContactRef, Relationship FROM dbo.HCM_PersonContact WHERE PersonRef = '" . $clientToken . "'");
        return $query->result();
    }

    /**
     * Get category type of carer
     * @param  [string] $id [CategoryID]
     * @return [string] $categoryName [ContactCategoryName]
     */
    public function getCategory($id)
    {
        $query = $this->db->query("SELECT ContactCategoryName FROM dbo.HCM_ContactCategory WHERE ContactCategoryID = '" . $id . "'");
        return $query->result();
    }
}