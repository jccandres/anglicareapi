<?php

class Client_model extends CI_model {
    public $db_ccm;

    public function __construct()
    {
        parent::__construct();
        // $this->db_ccm = $this->load->database("CCM_Live", TRUE);
        $this->load->model('carer_model');
        $this->load->model("Log_model");
    }

    public function getClients($top)
    {
        $top = ($top == NULL) ? "50" : $top;
        $query = $this->db->query("SELECT TOP $top ClientID, FirstName, LastName, Telephone1, Telephone2, Telephone3, DateOfBirth, PrimaryCarerRef FROM dbo.HCM_Client");
        $result = $query->result();
        foreach ($result as $key => $value) {
            $result[$key]->FirstName = trim($value->FirstName);
            $result[$key]->LastName = trim($value->LastName);
            $carerToken = ($this->getCarerToken($value->ClientID)) ? $this->getCarerToken($value->ClientID)[0]->ContactRef : "";
            $result[$key]->carerToken = $carerToken;
        }
        return $result;
    }

    /*
     * Get carer token in relational table of dbo.Client and dbo.Contact, dbo.PersonContact
     * Required parameters: clientToken/ClientID
     * PersonRef = ClientID
     * ContactRef = ContactID
     */
    public function getCarerToken($clientToken)
    {
        $query = $this->db->query("SELECT TOP 1 ContactRef FROM dbo.HCM_PersonContact WHERE PersonRef = '" . $clientToken . "'");
        return $query->result();
    }

    /**
     * [Get Info of a client including their carer]
     * @param  [string] $clientToken [Client ID]
     * @return [array]  $client_data [client info]
     */
    public function getClientInfo($clientToken, $userToken)
    {
        $client_data = $this->getClient($clientToken)[0];

        /* Get supervisor info */
        if ($client_data->SupervisorRef != NULL) {
            $temp = $this->getSupervisor($client_data->SupervisorRef)[0];
            $client_data->SupervisorName = trim($temp->FirstName) . " " . trim($temp->LastName);
            // $client_data->SupervisorPhone = $temp->Telephone1;
            // $client_data->SupervisorMobile = $temp->Telephone2;
            $client_data->SupervisorPhone = ($temp->Telephone1 != "") ? $temp->Telephone1 : $temp->Telephone2;
        }
        else {
            $client_data->SupervisorName = "";
            $client_data->SupervisorPhone = "";
            // $client_data->SupervisorMobile = "";
        }
        unset($client_data->SupervisorRef);

        /* Primary Carer */
        if ($client_data->PrimaryCarerRef == NULL) {
            $primaryCarerID = "";
        }
        else {
            $primaryCarerID = $this->carer_model->getPrimaryCarerToken($client_data->PrimaryCarerRef)[0]->ContactRef;
        }
        unset($client_data->PrimaryCarerRef);

        /* Get Carer Info */
        $temp_carer = $this->carer_model->getCarersToken($clientToken);
        foreach ($temp_carer as $key => $value) {
            $temp = $this->carer_model->getCarer($value->ContactRef);
            if (!empty($temp)) {

                /* Get Category/Contact Type */
                $contactType = $this->carer_model->getCategory($temp[0]->CategoryRef);
                $temp_carer[$key]->ContactType = trim($contactType[0]->ContactCategoryName);

                $temp_carer[$key]->ContactName = trim($temp[0]->FirstName) . " " . trim($temp[0]->LastName);
                // $temp_carer[$key]->ContactPhone = $temp[0]->Telephone1;
                // $temp_carer[$key]->ContactMobile = $temp[0]->Telephone2;
                $temp_carer[$key]->ContactPhone = ($temp[0]->Telephone1 != "") ? $temp[0]->Telephone1 : $temp[0]->Telephone2;
                $temp_carer[$key]->PrimaryCarer = ($primaryCarerID == $value->ContactRef) ? true : false;
            }
            else {
                unset($temp_carer[$key]);
            }
        }
        $temp_carer = array_values($temp_carer);
        $client_data->ClientContacts = $temp_carer;

        /* TODO Get all services available for a client */
        $client_data->Services = ['Transport', 'Social Support'];

        /**
        * Logs
        */
       if ($userToken) {
           $this->Log_model->add_log("view_profile", $userToken);
       }
        

        return $client_data;
    }

    /*
     * Get single client
     * Required parameters: clientToken/ClientID
     * Database: dbo.Client
     * Used in: getClientInfo
     */
    public function getClient($clientToken)
    {
        $query = $this->db->query("SELECT ClientID, Title, FirstName, LastName, Address, Town, County, PostCode, Telephone1, Telephone2, Telephone3, Telephone1Name, Telephone2Name, Telephone3Name, emailaddress, Status, SupervisorRef, PrimaryCarerRef FROM dbo.HCM_Client WHERE ClientID = '" . $clientToken . "'");
        $result = $query->result();
        foreach ($result as $key => $value) {
            $temp_home = array();
            $temp_mobile = array();
            if ($value->Telephone1Name == "Home" && $value->Telephone1 != "") {
                $temp_home[] = $value->Telephone1;
            }
            elseif ($value->Telephone1Name == "Mobile" && $value->Telephone1 != "") {
                $temp_mobile[] = $value->Telephone1;
            }

            if ($value->Telephone2Name == "Home" && $value->Telephone2 != "") {
                $temp_home[] = $value->Telephone2;
            }
            elseif ($value->Telephone2Name == "Mobile" && $value->Telephone2 != "") {
                $temp_mobile[] = $value->Telephone2;
            }

            if ($value->Telephone3Name == "Home" && $value->Telephone3 != "") {
                $temp_home[] = $value->Telephone3;
            }
            elseif ($value->Telephone3Name == "Mobile" && $value->Telephone3 != "") {
                $temp_mobile[] = $value->Telephone3;
            }

            $result[$key]->Home = implode(", ", $temp_home);
            $result[$key]->Mobile = implode(", ", $temp_mobile);
            $result[$key]->FirstName = trim($value->FirstName);
            $result[$key]->LastName = trim($value->LastName);

            unset($result[$key]->Telephone1);
            unset($result[$key]->Telephone1Name);
            unset($result[$key]->Telephone2);
            unset($result[$key]->Telephone2Name);
            unset($result[$key]->Telephone3);
            unset($result[$key]->Telephone3Name);
        }
        return $result;
    }

    /*
     * Get Supervisor of certain client
     * Required parameters: SupervisorRef
     * Database dbo.Worker
     * Used in: getClientInfo
     */
    public function getSupervisor($supervisorRef)
    {
        $query = $this->db->query("SELECT FirstName, LastName, Telephone1, Telephone2 FROM dbo.HCM_Worker WHERE WorkerID = '" . $supervisorRef . "'");
        $result = $query->result();
        foreach ($result as $key => $value) {
            $result[$key]->FirstName = trim($value->FirstName);
            $result[$key]->LastName = trim($value->LastName);
        }
        return $query->result();
    }
}