<?php

class Data_model extends CI_model {
    public $db_ccm;

    public function __construct()
    {
        parent::__construct();
        // $this->db_ccm = $this->load->database("CCM_Live", TRUE);
    }

    public function executeQuery($data)
    {
        $query = $this->db->query($data['query']);
        return $query;
    }

    public function getUsers()
    {
        // $this->db->query("ALTER TABLE dbo.Users ADD Mobile varchar(20)");
        $query = $this->db->query("SELECT * FROM dbo.Users ORDER BY CreatedTS DESC");
        return $query->result();
    }

    public function getDevices()
    {
        $query = $this->db->query("SELECT * FROM dbo.Devices");
        return $query->result();
    }

    public function getUsersDevices()
    {
        $query = $this->db->query("SELECT * FROM dbo.users_devices");
        return $query->result();
    }

    public function getSchedule()
    {
        $query = $this->db->query("SELECT * FROM dbo.Schedule");
        return $query->result();
    }

    public function getChangeVisit()
    {
        $query = $this->db->query("SELECT * FROM dbo.Change_Visit ORDER BY created_at DESC");
        return $query->result();
    }

    public function getCancelVisit()
    {
        $query = $this->db->query("SELECT * FROM dbo.Cancel_Visit");
        return $query->result();
    }

    public function getRequestVisit()
    {
        $query = $this->db->query("SELECT * FROM dbo.Request_Visit");
        return $query->result();
    }

    public function getRequestTransport()
    {
        $query = $this->db->query("SELECT * FROM dbo.Request_Transport");
        return $query->result();
    }

    public function getMessages()
    {
        $query = $this->db->query("SELECT * FROM dbo.Messages");
        return $query->result();
    }

    public function getNotification()
    {
        $query = $this->db->query("SELECT * FROM dbo.Notification");
        return $query->result();
    }

    public function getLogs()
    {
        $query = $this->db->query("SELECT * FROM dbo.Audit_Log ORDER BY CreatedAt DESC");
        return $query->result();
    }

    public function getUserByToken($userToken)
    {
        $query = $this->db->query("SELECT * FROM dbo.Users WHERE userID = '" . $userToken . "'");
        $result = $query->result();
        foreach ($result as $key => $value) {
            if ($value->carerID != "") {
                $query_carer = $this->db->query("SELECT * FROM dbo.HCM_Contact WHERE ContactID = '" . $value->carerID . "'");
                $result_carer = $query_carer->result()[0];
                $result[$key]->carer = $result_carer->FirstName . " " . $result_carer->LastName;
            }
            if ($value->clientID != "") {
                $query_client = $this->db->query("SELECT * FROM dbo.HCM_Client WHERE ClientID = '" . $value->clientID . "'");
                $result_client = $query_client->result()[0];
                $result[$key]->client = trim($result_client->FirstName) . " " . trim($result_client->LastName);
            }
        }
        return $result;
    }

    public function getBookingType($clientToken)
    {
        $query = $this->db->query("
            SELECT DISTINCT ee.PropertyValue AS BookingType 
            FROM dbo.HCM_Contract c 
            JOIN dbo.HCM_ExtendedEntity ee ON c.ContractID = ee.EntityID AND ee.EntityType = 'Contract' AND ee.PropertyName LIKE 'Booking Type%' 
            JOIN  dbo.HCM_VisitPlan vp ON c.ContractID = vp.ContractRef 
            WHERE LEN(ee.PropertyValue) > 1 AND vp.ClientRef = '".$clientToken."'  
            ");
        
        return $query->result();
    }

    public function testThis()
    {
        $query = $this->db->query("SELECT * FROM users_devices WHERE userID = 'adsd24fsdf' AND deviceID = 'asdasd'");
        var_dump($query->result());
        var_dump(empty($query->result())); die();
        $result = @$query->result()[0];
        var_dump($result->PIN); die();
    }
}