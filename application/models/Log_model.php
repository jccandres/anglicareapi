<?php

class Log_model extends CI_model {
    public $db_ccm;

    public function __construct()
    {
        parent::__construct();
        // $this->db_ccm = $this->load->database("CCM_Live", TRUE);
    }

    public function add_log($type, $userToken, $status = NULL, $deviceID = NULL)
    {
    	$query = $this->db->query("INSERT INTO dbo.Audit_Log (Type, UserID, Status, DeviceID) VALUES ('" . $type . "', '" . $userToken . "', '" . $status . "', '" .  $deviceID . "')");
    	return $query;
    }

    public function get_all()
    {
        $query = $this->db->query("SELECT * FROM dbo.Audit_Log WHERE UserID != '' ORDER BY CreatedAt DESC");
        $result = $query->result();
        foreach ($result as $key => $value) {
        	$result[$key]->Name = "";
        	if ($value->UserID != "") {
        		$result[$key]->Name = $this->get_user($value->UserID);
        	}
        }
        return $result;
    }

    public function get_user($userToken)
    {
    	$query = $this->db->query("SELECT * FROM dbo.Users WHERE userID = '" . $userToken . "'");
    	$result = $query->result()[0];
    	if ($result->type == "Client") {
    		$query = $this->db->query("SELECT * FROM dbo.HCM_Client WHERE ClientID = '" . $result->clientID . "'");
    	}
    	elseif ($result->type == "Carer") {
    		$query = $this->db->query("SELECT * FROM dbo.HCM_Contact WHERE ContactID = '" . $result->carerID . "'");
    	}
    	$user = $query->result()[0];
    	$name = trim($user->FirstName) . " " . trim($user->LastName);
    	return $name;
    }
}