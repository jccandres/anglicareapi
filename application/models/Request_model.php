<?php

date_default_timezone_set("Australia/Sydney");

class Request_model extends CI_model {
    public $db_ccm;

    public function __construct()
    {
        parent::__construct();
        // $this->db_ccm = $this->load->database("CCM_Live", TRUE);
        
        $this->load->model("Log_model");
    }

    function cmp($a, $b)
    {
        return strcmp($b->CreatedAt, $a->CreatedAt);
    }

    /**
     * Request new visit/schedule
     * @param  [array] $data [schedule]
     */
    public function requestVisit($data)
    {
        $CreatedAt = date("Y-m-d H:i:s");

        // $data['time'] = date("H:i:s", strtotime($data['time']));
        $data['date'] = date("Y-m-d", strtotime($data['date']));
        //htmlspecialchars($data['visitDescription'],ENT_QUOTES)
        $data['visitDescription'] = str_replace("'", "''", $data['visitDescription']);
        
        if (!empty($data)) {
            $query = $this->db->query("INSERT INTO dbo.Request_Visit (UserID, VisitDescription, Date, Time, Duration, CreatedAt) VALUES ('" . $data['userID'] . "', '" . $data['visitDescription'] . "', '" . $data['date'] . "', '" . $data['time'] . "', '" . $data['duration'] . "', '" . $CreatedAt . "')");
            // $query = $this->db->query("UPDATE dbo.Request_Visit SET VisitDescription = 'update test' WHERE UserID = 'C0A969A0E04615F640F550DEACA4F4F2' AND RequestVisitID = 'AC93F0EE-AB91-439C-AFED-0AE686E72178'");
            // die();
            /**
            * Logs
            */
            $this->Log_model->add_log("request_visit", $data['userID']);
            return $query;
        }
        else {
            return false;
        }
    }

    /**
     * Request new transport
     * @param  [array] $data [schedule]
     */
    public function requestTransport($data)
    {
        $CreatedAt = date("Y-m-d H:i:s");

        /* Converting date and time */
        $data['TransportDate'] = date("Y-m-d", strtotime($data['TransportDate']));
        $data['PickupTime'] = date("H:i:s", strtotime($data['PickupTime']));
        $data['AppointmentTime'] = date("H:i:s", strtotime($data['AppointmentTime']));
        $data['ReturnTime'] = date("H:i:s", strtotime($data['ReturnTime']));

        if (!empty($data)) {
            $query = $this->db->query("INSERT INTO 
                dbo.Request_Transport (
                UserID,
                ContactNo,
                TransportDate,
                PickupTime,
                ForAppointment,
                AppointmentTime,
                AppointmentPerson,
                PersonsTravelingWith,
                EquipmentToBeTransported,
                AssistanceRequire,
                PickupStreetAddress,
                PickupSuburb,
                PickupAddressInfo,
                DropoffStreetAddress,
                DropoffSuburb,
                DropoffAddressInfo,
                ReturnTrip,
                ReturnTime,
                ReturnStreetAddress,
                ReturnSuburb,
                ReturnAddressInfo,
                CreatedAt
                ) 
                VALUES (
                '" . $data['UserID'] . "',
                '" . $data['ContactNo'] . "',
                '" . $data['TransportDate'] . "',
                '" . $data['PickupTime'] . "',
                '" . $data['ForAppointment'] . "',
                '" . $data['AppointmentTime'] . "',
                '" . $data['AppointmentPerson'] . "',
                '" . $data['PersonsTravelingWith'] . "',
                '" . $data['EquipmentToBeTransported'] . "',
                '" . str_replace("'", "''", $data['AssistanceRequire']) ."',
                '" . $data['PickupStreetAddress'] . "',
                '" . $data['PickupSuburb'] . "',
                '" . str_replace("'", "''", $data['PickupAddressInfo']) ."',
                '" . $data['DropoffStreetAddress'] . "',
                '" . $data['DropoffSuburb'] . "',
                '" . str_replace("'", "''", $data['DropoffAddressInfo']) ."',
                '" . $data['ReturnTrip'] . "',
                '" . $data['ReturnTime'] . "',
                '" . $data['ReturnStreetAddress'] . "',
                '" . $data['ReturnSuburb'] . "',
                '" . str_replace("'", "''", $data['ReturnAddressInfo']) ."',
                '" . $CreatedAt . "'
                )");

            /**
            * Logs
            */
            $this->Log_model->add_log("request_transport", $data['UserID']);
            return $query;
        }
        else {
            return false;
        }
    }

    /**
     * Get list of requests - new request, cancel request, change request
     * @param  [type] $data [userID]
     * @return [type]       [description]
     */
    public function getRequestList($data)
    {
        // $arr_data = $this->getRequestVisit($data['userToken']);
        // $arr_data = $this->getRequestChange($data['userToken']);
        // $arr_data = $this->getRequestCancel($data['userToken']);
        $arr_data = array_merge($this->getRequestVisit($data['userToken']), $this->getRequestTransport($data['userToken']), $this->getRequestChange($data['userToken']), $this->getRequestCancel($data['userToken']));

        if ($arr_data) {
            usort($arr_data, array($this, 'cmp'));
        }
        /**
        * Logs
        */
        $this->Log_model->add_log("view_requests", $data['userToken']);
        return $arr_data;
    }

    public function getRequestVisit($userToken)
    {
        $query = $this->db->query("SELECT RequestVisitID AS ID, UserID, VisitDescription, Date, Time, Duration, CreatedAt, ME_Tk_ID AS RequestNo, ME_Tk_Status AS RequestStatus
            FROM dbo.Request_Visit WHERE UserID = '" . $userToken . "' AND Date >= GetDate() - 30");
        $result = $query->result();
        foreach ($result as $key => $value) {

            $tempHours = floor($value->Duration / 60);
            $tempMins = $value->Duration % 60;

            if ($tempHours == 1) {
                $string_hours = $tempHours . " hr";
            }
            elseif ($tempHours > 1) {
                $string_hours = $tempHours . " hrs";
            }
            else {
                $string_hours = "";
            }

            if ($tempMins == 1) {
                $string_mins = $tempMins . " min";
            }
            elseif ($tempMins > 1) {
                $string_mins = $tempMins . " mins";
            }
            else {
                $string_mins = "";
            }

            $result[$key]->Duration = trim($string_hours . " " . $string_mins);

            // $result[$key]->Duration = date("g \h\\r\s i \m\i\\n\s", mktime(0, $value->Duration));
            $result[$key]->DateSubmitted = date("l, d F Y", strtotime($value->CreatedAt));
            // unset($result[$key]->CreatedAt);
            $result[$key]->Time = date("g:i a", strtotime($value->Time));
            $result[$key]->Date = date("l, d F Y", strtotime($value->Date));
            $result[$key]->RequestType = "Request Visit";
            $result[$key]->PrevDate = "";
            $result[$key]->PrevTime = "";
            if (!$value->RequestNo) {
                $result[$key]->RequestNo = "";
            }
            
            if(!$value->RequestStatus){
                $result[$key]->RequestStatus = "";
            }else{
                if($value->RequestStatus == 'Open'){
                    $result[$key]->RequestStatus = "In Progress";
                }elseif ($value->RequestStatus == 'Closed'){
                    $result[$key]->RequestStatus = "Finalised";
                }
            }
        }
        return $result;
    }

    public function getRequestTransport($userToken)
    {
        $data = array();
        $query = $this->db->query("SELECT * FROM dbo.Request_Transport WHERE UserID = '" . $userToken . "' AND TransportDate >= GetDate() - 30");
        $result = $query->result();
        $ctr = 0;
        foreach ($result as $key => $value) {
            $data[$ctr] = new \stdClass();
            $data[$ctr]->ID = $value->RequestTransportID;
            $data[$ctr]->UserID = $value->UserID;
            $data[$ctr]->RequestType = "Request Transport";
            $data[$ctr]->DateSubmitted = date("l, d F Y", strtotime($value->CreatedAt));
            $data[$ctr]->PreferredDate = date("l, d F Y", strtotime($value->TransportDate));
            $data[$ctr]->PreferredTime = date("g:i a", strtotime($value->PickupTime));
            $data[$ctr]->PickupSuburb = $value->PickupSuburb;
            $data[$ctr]->DropoffSuburb = $value->DropoffSuburb;
            $data[$ctr]->ReturnTrip = ($value->ReturnTrip) ? "Yes" : "No";
            $data[$ctr]->CreatedAt = $value->CreatedAt;
            $data[$ctr]->Date = date("l, d F Y", strtotime($value->TransportDate));
            $data[$ctr]->Time = date("g:i a", strtotime($value->PickupTime));
            $data[$ctr]->PrevDate = "";
            $data[$ctr]->PrevTime = "";
            $data[$ctr]->RequestNo = ($value->ME_Tk_ID) ? $value->ME_Tk_ID : "";
            //$data[$ctr]->RequestStatus = ($value->ME_Tk_Status) ? $value->ME_Tk_Status : "";
            if(!$value->ME_Tk_Status){
                $data[$ctr]->RequestStatus = "";
            }else{
                if($value->ME_Tk_Status == 'Open'){
                    $data[$ctr]->RequestStatus = "In Progress";
                }elseif($value->ME_Tk_Status == 'Closed') {
                    $data[$ctr]->RequestStatus = "Finalised";
                }else{
                    $data[$ctr]->RequestStatus = $value->ME_Tk_Status;
                }
            }
            $ctr++;
        }
        return $data;
    }

    public function getRequestChange($userToken)
    {
        $query = $this->db->query("SELECT
            Change_Visit.ChangeVisitID AS ID,
            Change_Visit.UserID AS UserID,
            Change_Visit.Created_at AS DateSubmitted,
            Change_Visit.time,
            Change_Visit.date,
            Change_Visit.duration,
            APP_ClientSchedule.VisitTypeName,
            APP_ClientSchedule.Service,
            APP_ClientSchedule.StartTime,
            Change_Visit.ME_Tk_ID AS RequestNo,
            Change_Visit.ME_Tk_Status AS RequestStatus
            FROM dbo.Change_Visit 
            INNER JOIN dbo.APP_ClientSchedule ON Change_Visit.visitID = APP_ClientSchedule.VisitID
            WHERE Change_Visit.userID = '" . $userToken . "' AND Change_Visit.visitID != '' AND date >= GetDate() - 30");
        $result = $query->result();
        foreach ($result as $key => $value) {
            $result[$key]->VisitDescription = trim($value->VisitTypeName) . "\n" . trim($value->Service);
            unset($result[$key]->VisitTypeName);
            unset($result[$key]->Service);
            
            $tempHours = floor($value->duration / 60);
            $tempMins = $value->duration % 60;

            if ($tempHours == 1) {
                $string_hours = $tempHours . " hr";
            }
            elseif ($tempHours > 1) {
                $string_hours = $tempHours . " hrs";
            }
            else {
                $string_hours = "";
            }

            if ($tempMins == 1) {
                $string_mins = $tempMins . " min";
            }
            elseif ($tempMins > 1) {
                $string_mins = $tempMins . " mins";
            }
            else {
                $string_mins = "";
            }

            $result[$key]->Duration = trim($string_hours . " " . $string_mins);
            unset($result[$key]->duration);
            $result[$key]->Date = date("l, d F Y", strtotime($value->date));
            unset($result[$key]->date);
            //$result[$key]->Time = $value->time;
            $result[$key]->Time = date("g:i a", strtotime($value->time));
            // $result[$key]->Time = date("h:i a", strtotime($value->time));
            unset($result[$key]->time);
            $result[$key]->PrevDate = date("l, d F Y", strtotime($value->StartTime));
            $result[$key]->PrevTime = date("g:i a", strtotime($value->StartTime));
            unset($result[$key]->StartTime);
            $result[$key]->RequestType = "Request Change";
            $result[$key]->CreatedAt = $value->DateSubmitted;
            $result[$key]->DateSubmitted = date("l, d F Y", strtotime($value->DateSubmitted));
            if (!$value->RequestNo) {
                $result[$key]->RequestNo = "";
            }
            if(!$value->RequestStatus){
                $result[$key]->RequestStatus = "";
            }else{
                if($value->RequestStatus == 'Open'){
                    $result[$key]->RequestStatus = "In Progress";
                }elseif($value->RequestStatus == 'Closed'){
                    $result[$key]->RequestStatus = "Finalised";
                }
            }
        }
        return $result;
    }

    public function getRequestCancel($userToken)
    {
        $query = $this->db->query("SELECT 
            Cancel_Visit.cancelVisitID AS ID,
            Cancel_Visit.userID AS UserID,
            Cancel_Visit.created_at AS DateSubmitted,
            APP_ClientSchedule.VisitTypeName,
            APP_ClientSchedule.Service,
            APP_ClientSchedule.StartTime,
            APP_ClientSchedule.Duration,
            Cancel_Visit.ME_Tk_ID AS RequestNo,
            Cancel_Visit.ME_Tk_Status AS RequestStatus
            FROM dbo.Cancel_Visit
            INNER JOIN dbo.APP_ClientSchedule ON Cancel_Visit.visitID = APP_ClientSchedule.VisitID
            WHERE Cancel_Visit.userID = '" . $userToken . "' AND created_at >= GetDate() - 30");
        $result = $query->result();
        foreach ($result as $key => $value) {
            $result[$key]->VisitDescription = trim($value->VisitTypeName) . "\n" . trim($value->Service);
            unset($result[$key]->VisitTypeName);
            unset($result[$key]->Service);
            //$result[$key]->Duration = date("g \h\\r\s i \m\i\\n\s", mktime(0, $value->Duration));

            $tempHours = floor($value->Duration / 60);
            $tempMins = $value->Duration % 60;

            if ($tempHours == 1) {
                $string_hours = $tempHours . " hr";
            }
            elseif ($tempHours > 1) {
                $string_hours = $tempHours . " hrs";
            }
            else {
                $string_hours = "";
            }

            if ($tempMins == 1) {
                $string_mins = $tempMins . " min";
            }
            elseif ($tempMins > 1) {
                $string_mins = $tempMins . " mins";
            }
            else {
                $string_mins = "";
            }

            $result[$key]->Duration = trim($string_hours . " " . $string_mins);

            $result[$key]->Date = date("l, d F Y", strtotime($value->StartTime));
            $result[$key]->Time = date("g:i a", strtotime($value->StartTime));
            $result[$key]->PrevDate = "";
            $result[$key]->PrevTime = "";
            unset($result[$key]->StartTime);
            $result[$key]->RequestType = "Request Cancel";
            $result[$key]->CreatedAt = $value->DateSubmitted;
            $result[$key]->DateSubmitted = date("l, d F Y", strtotime($value->DateSubmitted));
            if (!$value->RequestNo) {
                $result[$key]->RequestNo = "";
            }
            if(!$value->RequestStatus){
                $result[$key]->RequestStatus = "";
            }else{
                if($value->RequestStatus == 'Open'){
                    $result[$key]->RequestStatus = "In Progress";
                }elseif($value->RequestStatus == 'Closed'){
                    $result[$key]->RequestStatus = "Finalised";
                }
            }
            
        }
        return $result;
    }
}