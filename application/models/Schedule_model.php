<?php

date_default_timezone_set("Australia/Sydney");

class Schedule_model extends CI_model {
    public $db_ccm;

    public function __construct()
    {
        parent::__construct();
        // $this->db_ccm = $this->load->database("CCM_Live", TRUE);
        
        $this->load->model("Log_model");
    }

    public function getSchedule($data)
    {

        if (!empty($data)) {
            
            $queryNewID = $this->db->query("SELECT DISTINCT NewClientID FROM dbo.APP_ClientSchedule 
                WHERE (ExceptionTypeRef NOT IN
                        ('E17E0BCB-028F-456E-95FE-526CD9580BB4',
                        '8E515602-7E30-449B-8CA5-E3D9F3CEFC91',
                        '16776758-942F-4DD2-91F8-0BD13B6E99EC',
                        'D238B3A7-985A-49E9-81D2-2CFE14317FDA',
                        '5A457E19-6265-4436-9160-5208D69A5845',
                        '99E90046-E396-4740-9090-63798F54AE81')
                        OR exceptiontypeRef is NULL)
                    AND ClientID = '" . $data['ClientToken'] . "'"); //startTime BETWEEN GetDate() - 14 and GetDate() + 14 add this line AND ShiftRef is not null

            $resultNewID = $queryNewID->result();
            
            $newIDs = array();
            
            foreach ($resultNewID as $key => $value) {
                $newIDs[] = $value->NewClientID; 
            }
            
            $extension = "";
            
            //print_r($newIDs);
            
            foreach($newIDs as $newID){
                $extension .= " OR NewClientID = '".$newID."'";
            }
            
            $query = $this->db->query("SELECT VisitID, ClientID, FirstName, LastName, StartTime, VisitTypeName, Service, Worker, Duration FROM dbo.APP_ClientSchedule 
                WHERE (ExceptionTypeRef NOT IN
                        ('E17E0BCB-028F-456E-95FE-526CD9580BB4',
                        '8E515602-7E30-449B-8CA5-E3D9F3CEFC91',
                        '16776758-942F-4DD2-91F8-0BD13B6E99EC',
                        'D238B3A7-985A-49E9-81D2-2CFE14317FDA',
                        '5A457E19-6265-4436-9160-5208D69A5845',
                        '99E90046-E396-4740-9090-63798F54AE81')
                        OR exceptiontypeRef is NULL)
                    AND (ClientID = '" . $data['ClientToken'] . "'".$extension.")
                    AND StartTime BETWEEN GetDate() AND GetDate() + 14 ORDER BY StartTime ASC"); //startTime BETWEEN GetDate() - 14 and GetDate() + 14 add this line AND ShiftRef is not null

            $result = $query->result();
            
            $filtered_result = array();
            $tempSched = new stdClass();
            $tempSched->StartTime = "";
            $tempSched->Worker = "";
            $tempSched->Duration = "";
            
            foreach ($result as $key => $value) {
            
                $result[$key]->VisitDescription = trim($value->VisitTypeName) . "\n" . trim($value->Service);
                unset($result[$key]->VisitTypeName);
                unset($result[$key]->Service);
                $result[$key]->FirstName = trim($value->FirstName);
                $result[$key]->LastName = trim($value->LastName);
                $result[$key]->Worker = trim($value->Worker);
                // $tempHours = date("g", mktime(0, $value->Duration));
                // $tempMins = date("i", mktime(0, $value->Duration));
                $tempHours = floor($value->Duration / 60);
                $tempMins = $value->Duration % 60;

                if ($tempHours == 1) {
                    $string_hours = $tempHours . " hr";
                }
                elseif ($tempHours > 1) {
                    $string_hours = $tempHours . " hrs";
                }
                else {
                    $string_hours = "";
                }

                if ($tempMins == 1) {
                    $string_mins = $tempMins . " min";
                }
                elseif ($tempMins > 1) {
                    $string_mins = $tempMins . " mins";
                }
                else {
                    $string_mins = "";
                }

                $result[$key]->Duration = trim($string_hours . " " . $string_mins);
                // $result[$key]->Duration = date("g \h\\r\s i \m\i\\n\s", mktime(0, $value->Duration));
                $result[$key]->Date = date("l, d F Y", strtotime($value->StartTime));
                $result[$key]->Time = date("g:i a", strtotime($value->StartTime));
                $request_status = "";
                if ($this->isVisitInChangeRequest($value->VisitID)) {
                    $request_status = "Change";
                }
                elseif ($this->isVisitInCancelRequest($value->VisitID)) {
                    $request_status = "Cancel";
                }
                $result[$key]->Status = $request_status;
                // $result[$key]->Status  = ($this->isVisitInChangeRequest($value->VisitID) || $this->isVisitInCancelRequest($value->VisitID)) ? "Pending" : "";
                $result[$key]->StartTime = date("Y-m-d H:i:s", strtotime($value->StartTime));
                
                if($tempSched->StartTime != $result[$key]->StartTime || $tempSched->Worker != $result[$key]->Worker || $tempSched->Duration != $result[$key]->Duration){
                    $filtered_result[] = $result[$key];
                    $tempSched = $result[$key];
                }
                
            }

            /**
            * Logs
            */
            if (@$data['UserToken']) {
               $this->Log_model->add_log("view_visits", $data['UserToken']);
            }

            return $filtered_result;
        }
        else {
            return false;
        }
    }

    public function isVisitInChangeRequest($visitID)
    {
        $query = $this->db->query("SELECT COUNT(*) AS visitCount FROM dbo.Change_Visit WHERE visitID = '$visitID' AND (ME_Tk_Status = 'New' OR ME_Tk_Status = 'Open')");
        return $query->result()[0]->visitCount;
    }

    public function isVisitInCancelRequest($visitID)
    {
        $query = $this->db->query("SELECT COUNT(*) AS visitCount FROM dbo.Cancel_Visit WHERE visitID = '$visitID' AND NOT ME_Tk_Status = 'Closed'");
        return $query->result()[0]->visitCount;
    }

    public function getSchedule2($data)
    {
        if (!empty($data)) {
            $query = $this->db_ccm->query("
                SELECT Visit.VisitID, Client.ClientID, Client.FirstName, Client.LastName, Visit.StartTime,
                CASE WHEN Visit.Description = '' THEN VisitType.VisitTypeName ELSE Visit.Description END as VisitDescription,
                CASE WHEN Worker.FirstName = '' THEN Worker.LastName ELSE Worker.FirstName END as Worker,
                Visit.Duration
                FROM dbo.Visit
                JOIN dbo.VisitType ON Visit.VisitTypeRef = VisitType.VisitTypeID
                INNER JOIN dbo.Worker ON Visit.WorkerRef = Worker.WorkerID
                INNER JOIN dbo.Client ON Visit.ClientRef = Client.ClientID
                LEFT JOIN dbo.ExceptionType ON Visit.ExceptionTypeRef = ExceptionType.ExceptionTypeID
                WHERE (Visit.ExceptionTypeRef NOT IN
                        ('E17E0BCB-028F-456E-95FE-526CD9580BB4',
                        '8E515602-7E30-449B-8CA5-E3D9F3CEFC91',
                        '16776758-942F-4DD2-91F8-0BD13B6E99EC',
                        'D238B3A7-985A-49E9-81D2-2CFE14317FDA',
                        '5A457E19-6265-4436-9160-5208D69A5845',
                        '99E90046-E396-4740-9090-63798F54AE81')
                        OR exceptiontypeRef is Null)
                    AND ShiftRef is not null
                    AND Client.ClientID = '" . $data['ClientToken'] . "' 
                ORDER BY Visit.StartTime DESC
                "); //startTime BETWEEN GetDate() - 14 and GetDate() + 14 add this line

            $result = $query->result();
            foreach ($result as $key => $value) {
                $result[$key]->FirstName = trim($value->FirstName);
                $result[$key]->LastName = trim($value->LastName);
                $result[$key]->Worker = trim($value->Worker);
                $result[$key]->Duration = date("g \h\\r\s i \m\i\\n\s", mktime(0, $value->Duration));
                $result[$key]->Date = date("l, d F Y", strtotime($value->StartTime));
                $result[$key]->Time = date("h:i a", strtotime($value->StartTime));
                // unset($result[$key]->StartTime);
            }
            return $result;
        }
        else {
            return false;
        }
    }

    /**
     * Request Change Visit
     * @param  [array] $data []
     * @return [bool]       []
     */
    public function changeVisit($data)
    {
        $created_at = date("Y-m-d H:i:s");

        // $data['time'] = date("H:i:s", strtotime($data['time']));
        $data['date'] = date("Y-m-d", strtotime($data['date']));

        if (!empty($data)) {
            $query = $this->db->query("INSERT INTO dbo.Change_Visit (userID, visitID, visitDescription, date, time, duration, created_at, ME_Tk_Status, ME_Tk_ID) VALUES ('" . $data['userID'] . "', '" . $data['visitID'] . "', '" . htmlspecialchars($data['visitDescription'], ENT_QUOTES) . "', '" . $data['date'] . "', CAST('" . $data['time'] . "' AS varchar(50)), '" . $data['duration'] . "', '" . $created_at . "', 'New', 'Allocating')");

            /**
            * Logs
            */
            $this->Log_model->add_log("change_visit", $data['userID']);

            return $query;
        }
        else {
            return false;
        }
    }

    /**
     * Request Cancel Visit
     * @param  [array] $data []
     * @return [bool]       []
     */
    public function cancelVisit($data)
    {
        $created_at = date("Y-m-d H:i:s");

        if (!empty($data)) {
            $query = $this->db->query("INSERT INTO dbo.Cancel_Visit (userID, visitID, created_at, ME_Tk_Status, ME_Tk_ID) VALUES ('" . $data['userID'] . "', '" . $data['visitID'] . "', '" . $created_at . "', 'New', 'Allocating')");
            /**
            * Logs
            */
            $this->Log_model->add_log("cancel_visit", $data['userID']);
            return $query;
        }
        else {
            return false;
        }
    }

    /**
     * Check Schedule ID if existing
     * @param  [string] $id [schedule id]
     * @return [int]     [result count]
     */
    public function checkScheduleID($id)
    {
        $query = $this->db->query("SELECT COUNT(*) AS scheduleCount FROM dbo.Schedule WHERE scheduleID = '$id'");
        return $query->result()[0]->scheduleCount;
    }

}