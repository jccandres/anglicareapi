<?php

date_default_timezone_set("Australia/Sydney");

class User_model extends CI_model {
    public $db_ccm;

    public function __construct()
    {
        parent::__construct();
        // $this->db_ccm = $this->load->database("CCM_Live", TRUE);
        $this->load->model('client_model');
        $this->load->model("Log_model");
    }

    /*
     * Get from dbo.Users
     * Return: userID, clientID
     */
    public function verifyUser($type, $clientID, $carerID = "")
    {
        $query = $this->db->query("SELECT userID, clientID FROM dbo.Users WHERE type = '" . $type . "' AND carerID = '" . $carerID . "' AND clientID = '" . $clientID . "'");
        return $query->result();
    }

    /*
     * Check userID if existing in dbo.Users
     */
    public function checkUserId($userID)
    {
        $query = $this->db->query("SELECT COUNT(*) AS userCount FROM dbo.Users WHERE userID = '$userID'");
        return $query->result()[0]->userCount;
    }

    /*
     * Insert into dbo.Users
     */
    public function insertUser($type, $clientID, $mobile, $carerID = "")
    {
        $userID = generateRandomString();

        while ($this->checkUserId($userID)) {
            $userID = generateRandomString();
        }
        $query = $this->db->query("INSERT INTO dbo.Users (userID, type, carerID, clientID, PIN, Mobile) VALUES ('$userID', '$type', '$carerID', '$clientID', '', '$mobile')");
        return $userID;
    }

    /*
     * Get data from dbo.Users by userID
     */
    public function getUserByUserId($userID)
    {
        $query = $this->db->query("SELECT * FROM dbo.Users WHERE userID = '" . $userID . "'");
        return $query->result();
    }

    public function getUser($data)
    {
        /**
         * Check if phone number starts with '04' and is 10 digit
         * return false if not
         * removed (07-12-18)
         */
        // $temp_phone = str_replace(' ', '', $data['Phone']);
        // if ( substr($temp_phone, 0, 2) != "04" || strlen($temp_phone) != 10) {
        //     return false;
        // }
        $temp_phone = 0;
        if ($data['ContactType'] == "Carer") {

            if ($this->getCarerTokenAsCarer($data)) {
                $carerToken = $this->getCarerTokenAsCarer($data)[0]->carerToken;
            }
            else {
                $result['message'] = "No Carer Data Found";
                $result['status'] = false;
                return $result;
            }
            
            $temp_client_data = $this->getClientTokenAsCarer($data);
            if ($temp_client_data) {
                $clientToken = $temp_client_data[0]->clientToken;
                /* Remove to allow user to register even if client have no supervisor #jc */
                /*if (!$temp_client_data[0]->SupervisorRef) {
                    return false;
                }*/
            }
            else {
                $result['message'] = "No Client Data Found";
                $result['status'] = false;
                return $result;
            }

            $user = $this->verifyUser("Carer", $clientToken, $carerToken);
            if (empty($user)) {
                $userToken = $this->insertUser("Carer", $clientToken, $temp_phone, $carerToken);
                // $this->sendSMSVerificationCode($userToken);
            }
            else {
                $userToken = $user[0]->userID;
                $clientToken = $user[0]->clientID;
                $this->updateUserMobile($temp_phone, $userToken);
            }
            /**
             * Add Temporary Again(05-21-2018)
             * To Allow Next of Kin
             */
            $result['userToken'] = $userToken;
            $result['clientToken'] = $clientToken;
            $result['status'] = true;

            /**
             * Logs
             */
            $this->Log_model->add_log("register", $userToken, "Carer");

            return $result;
            
            /**
             * Remove Temporary Again(05-21-2018)
             * To Allow Next of Kin
             */
            /*$temp = $this->checkUser($carerToken, $clientToken)->countUser;
            if ($temp) {
                $result['userToken'] = $userToken;
                $result['clientToken'] = $clientToken;
                return $result;
            }
            else {
                return false;
            }*/
        }
        elseif ($data['ContactType'] == "Client") {

            $temp_client_data = $this->getClientTokenAsClient($data);

            if ($temp_client_data) {
                $clientToken = $temp_client_data[0]->clientToken;
                /* Remove to allow user to register even if client have no supervisor #jc */
                // if (!$temp_client_data[0]->SupervisorRef) {
                //     return false;
                // }
            }
            else {
                $result['message'] = "No Client Data Found";
                $result['status'] = false;
                return $result;
            }
            
            $user = $this->verifyUser("Client", $clientToken);
            
            if (empty($user)) {
                $userToken = $this->insertUser("Client", $clientToken, $temp_phone);
                // $this->sendSMSVerificationCode($userToken);
            }
            else {
                $userToken = $user[0]->userID;
                $clientToken = $user[0]->clientID;
                $this->updateUserMobile($temp_phone, $userToken);
            }

            $result['userToken'] = $userToken;
            $result['clientToken'] = $clientToken;
            $result['status'] = true;

            /**
             * Logs
             */
            $this->Log_model->add_log("register", $userToken, "Client");

            return $result;
        }
        else {
            $result['status'] = false;
            return $result;
        }
    }

    /*
     * Check if client is under the correct carer
     * Required parameters: carerToken, clientToken
     * Database: dbo.PersonContact
     */
    public function checkUser($carerToken, $clientToken)
    {
        $query = $this->db->query("SELECT COUNT(*) as countUser FROM dbo.HCM_PersonContact WHERE ContactRef = '" . $carerToken . "' AND PersonRef = '" . $clientToken . "'");
        return $query->result()[0];
    }

    /*
     * Get carer token as carer
     * Required parameters: FirstName, LastName, Phone
     * Database: dbo.Contact
     */
    public function getCarerTokenAsCarer($data) //Carer
    {
        // $query = $this->db->query("SELECT TOP 1 ContactID AS carerToken FROM dbo.HCM_Contact 
        //     WHERE FirstName = '" . $data['FirstName'] . "' 
        //     AND LastName = '" . $data['LastName'] . "' 
        //     AND (REPLACE(REPLACE(REPLACE(Telephone1, ' ', ''), '.', ''), '-', '') LIKE '%" . str_replace(' ', '', $data['Phone']) . "%' 
        //     OR REPLACE(REPLACE(REPLACE(Telephone2, ' ', ''), '.', ''), '-', '') LIKE '%" . str_replace(' ', '', $data['Phone']) . "%' 
        //     OR REPLACE(REPLACE(REPLACE(Telephone3, ' ', ''), '.', ''), '-', '') LIKE '%" . str_replace(' ', '', $data['Phone']) . "%')");
        $query = $this->db->query("SELECT TOP 1 ContactID AS carerToken FROM dbo.HCM_Contact 
            WHERE FirstName = '" . $data['FirstName'] . "' 
            AND LastName = '" . $data['LastName'] . "'");
        return $query->result();
    }

    /*
     * Get client token as carer
     * Required parameters: ClientFirstName, DateOfBirthOfClient
     * Database: dbo.Client
     */
    public function getClientTokenAsCarer($data)
    {
        $dateOfBirth = date("Y-m-d", strtotime($data['DateOfBirth']));
        $query = $this->db->query("SELECT TOP 1 ClientID AS clientToken, SupervisorRef FROM dbo.HCM_Client 
            WHERE FirstName = '" . $data['ClientFirstName'] . "' 
            AND DateOfBirth = '" . $dateOfBirth . "'
            AND Status = 1");
        return $query->result();
    }

    /*
     * Get client token as client
     * Required parameters: FirstName, LastName, Phone
     * Database: dbo.Client
     */
    public function getClientTokenAsClient($data)
    {
        $dateOfBirth = date("Y-m-d", strtotime($data['DateOfBirth']));
        $query = $this->db->query("SELECT TOP 1 ClientID AS clientToken, SupervisorRef FROM dbo.HCM_Client 
            WHERE FirstName = '" . $data['FirstName'] . "' 
            AND LastName = '" . $data['LastName'] . "' 
            AND DateOfBirth = '" . $dateOfBirth . "' 
            AND Status = 1");
        return $query->result();
    }

    /*
     * Update User's data
     * Passed data: userToken, PIN, deviceID, deviceName, firebaseKey, notification
     */
    public function updateUserData($data)
    {
        /**
         * Update PIN
         * Removed (07-25-2018 jc)
         * PIN is saved to users_devices table, from users table
         */
        // if (!$this->updatePIN($data['userToken'], $data['PIN'])) {
        //     return false;
        // }

        $data['deviceName'] = str_replace("'", "''", $data['deviceName']);
        
        /* Register a Device */
        if (!$this->isDeviceRegistered($data['deviceID']))
        {
            $this->insertDevice($data['deviceID'], $data['deviceName'], $data['firebaseKey']);
        }
        else {
            $this->updateDevice($data['deviceID'], $data['deviceName'], $data['firebaseKey']);
        }

        /* Link user to device */
        if (!$this->isDeviceLinkToUser($data['userToken'], $data['deviceID'])) {
            /**
             * 07-25-2018 jc
             * Added PIN
             */
            $this->linkDevice($data['userToken'], $data['deviceID'], $data['notification'], $data['PIN']);
        }
        else {
            /**
             * 07-25-2018 jc
             * Added PIN
             */
            $this->updateUserDevice($data['userToken'], $data['deviceID'], $data['notification'], $data['PIN']);
        }

        /**
        * Logs
        */
        $this->Log_model->add_log("register_pin", $data['userToken']);

        return true;
    }

    public function isDeviceRegistered($deviceID)
    {
        $query = $this->db->query("SELECT COUNT(*) AS deviceCount FROM dbo.Devices WHERE deviceID = '" . $deviceID . "'");
        return $query->result()[0]->deviceCount;
    }

    public function isDeviceLinkToUser($userToken, $deviceID)
    {
        $query = $this->db->query("SELECT COUNT(*) AS deviceCount FROM dbo.users_devices WHERE userID = '" . $userToken . "' AND deviceID = '" . $deviceID . "'");
        return $query->result()[0]->deviceCount;
    }

    public function insertDevice($deviceID, $deviceName, $firebaseKey)
    {
        $query = $this->db->query("INSERT INTO dbo.Devices (deviceID, name, firebaseKey) VALUES ('" . $deviceID . "', '" . $deviceName . "', '" . $firebaseKey . "')");
        return $query;
    }

    public function updateDevice($deviceID, $deviceName, $firebaseKey)
    {
        $query = $this->db->query("UPDATE dbo.Devices SET name = '" . $deviceName . "', firebaseKey = '" . $firebaseKey . "' WHERE deviceID = '" . $deviceID . "'");
        return $query;
    }

    /**
     * 07-25-2018 jc
     * Added PIN
     */
    public function linkDevice($userToken, $deviceID, $notification, $PIN)
    {
        $query = $this->db->query("INSERT INTO dbo.users_devices (userID, deviceID, notification, isactive, PIN) VALUES ('" . $userToken . "', '" . $deviceID . "', '" . $notification . "', 1, '" . $PIN . "')");
        return $query;
    }

    public function updateUserDevice($userToken, $deviceID, $notification, $PIN)
    {
        $query = $this->db->query("UPDATE dbo.users_devices SET notification = '" . $notification . "', isactive = 1, PIN = '" . $PIN . "' WHERE userID = '" . $userToken . "' AND deviceID = '" . $deviceID . "'");
        return $query;
    }

    public function updateUsersPin($userToken, $PIN)
    {
        $query = $this->db->query("UPDATE dbo.Users SET PIN = '" . $PIN . "' WHERE userID = '" . $userToken . "'");
        return $query;
    }

    /**
     * Update PIN
     * Removed (07-25-2018 jc)
     * PIN is saved to users_devices table, from users table
     */
    // public function updatePIN($userToken, $pin)
    // {
    //     $query = $this->db->query("UPDATE dbo.Users SET PIN = '" . $pin . "' WHERE userID = '" . $userToken . "'");
    //     return $query;
    // }
    
    /**
     * New update PIN
     * (07-25-2018 jc)
     */
    public function updatePIN($userToken, $deviceID, $PIN)
    {
        $query = $this->db->query("UPDATE dbo.users_devices SET PIN = '" . $PIN . "' WHERE userID = '" . $userToken . "' AND deviceID = '" . $deviceID . "'");
        return $query;
    }

    /*
     * Update firebase key
     * Data: deviceID, firebaseKey
     * Return: bool
     */
    public function updateFirebaseKey($data)
    {
        $query = $this->db->query("UPDATE dbo.Devices SET firebaseKey = '" . $data['firebaseKey'] . "' WHERE deviceID = '" . $data['deviceID'] . "'");
        return $query;
    }

    /*
     * Validate PIN
     * Data: userToken, PIN
     * Return: int
     */
    public function validatePIN($data)
    {
        $result = array();
        if (@$data['deviceID']) {
            $query = $this->db->query("SELECT * FROM dbo.users_devices WHERE userID = '" . $data['userToken'] . "' AND deviceID = '" . $data['deviceID'] . "'");
            $result = $query->result();
        }

        if ( !empty($result) && ($result[0]->PIN == $data['PIN']) ) {
            $return_result = true;
        }
        else {
            $query_users = $this->db->query("SELECT * FROM dbo.Users WHERE userID = '" . $data['userToken'] . "' AND PIN = '" . $data['PIN'] . "'");
            $pin_users = $query_users->result();
            if ($pin_users) {
                if (@$data['deviceID']) {
                    $this->updatePIN($data['userToken'], $data['deviceID'], $data['PIN']);
                }
                $return_result = true;
            }
            else {
                $return_result = false;
            }
        }

        // $result = $query->result()[0];
        // if ($result->PIN == $data['PIN']) {
        //     $return_result = true;
        // }
        // elseif ($result->PIN == NULL) {
        //     $query_users = $this->db->query("SELECT * FROM dbo.Users WHERE userID = '" . $data['userToken'] . "' AND PIN = '" . $data['PIN'] . "'");
        //     $pin_users = $query_users->result();
        //     if ($pin_users) {
        //         $this->updatePIN($data['userToken'], $data['deviceID'], $data['PIN']);
        //         $return_result = true;
        //     }
        //     else {
        //         $return_result = false;
        //     }
        // }
        // else {
        //     $return_result = false;
        // }

        /**
        * Logs
        */
        if ($return_result) {
            $log_status = "success";
        }
        else {
            $log_status = "fail";
        }
        $this->Log_model->add_log("validate_pin", $data['userToken'], $log_status);

        return $return_result;
    }

    /*
     * Unlink user
     * Data: userToken, deviceID
     */
    public function unlinkUser($data)
    {
        $query = $this->db->query("UPDATE dbo.users_devices SET isactive = 0 WHERE userID = '" . $data['userToken'] . "' AND deviceID = '" . $data['deviceID'] . "'");
        /**
        * Logs
        */
        $this->Log_model->add_log("unlink_user", $data['userToken']);
        return $query;
    }

    /*
     * Get Booking type of each client
     */
    public function getBookingType($clientToken)
    {
        // $query = $this->db->query("
        //     SELECT DISTINCT HCM_ExtendedEntity.PropertyValue AS BookingType
        //     FROM dbo.HCM_VisitType
        //     JOIN dbo.HCM_ExtendedEntity ON HCM_VisitType.VisitTypeID = HCM_ExtendedEntity.EntityID AND HCM_ExtendedEntity.EntityType = 'VisitType' AND HCM_ExtendedEntity.PropertyName = 'Booking Type'
        //     JOIN dbo.HCM_VisitPlanVisit ON HCM_VisitType.VisitTypeID = HCM_VisitPlanVisit.VisitTypeRef
        //     JOIN dbo.HCM_VisitPlan ON HCM_VisitPlanVisit.VisitPlanRef = HCM_VisitPlan.VisitPlanID
        //     WHERE HCM_VisitPlan.ClientRef = '" . $clientToken . "'
        //     ");
        
        $query = $this->db->query("
            SELECT DISTINCT ee.PropertyValue AS BookingType 
            FROM dbo.HCM_Contract c 
            JOIN dbo.HCM_ExtendedEntity ee ON c.ContractID = ee.EntityID AND ee.EntityType = 'Contract' AND ee.PropertyName LIKE 'Booking Type%' 
            JOIN  dbo.HCM_VisitPlan vp ON c.ContractID = vp.ContractRef 
            WHERE LEN(ee.PropertyValue) > 1 AND vp.ClientRef = '".$clientToken."'  
            ");
        
        //SELECT DISTINCT HCM_ExtendedEntity.PropertyValue AS BookingType
        //     FROM dbo.HCM_Contract
//             JOIN dbo.HCM_ExtendedEntity ON HCM_Contract.ContractID = HCM_ExtendedEntity.EntityID
//                 AND HCM_ExtendedEntity.EntityType = 'Contract'
//                 AND HCM_ExtendedEntity.PropertyName LIKE 'Booking Type%'
//             JOIN HCM_VisitPlan ON HCM_Contract.ContractID = HCM_VisitPlan.ContractRef
//             WHERE LEN(HCM_ExtendedEntity.PropertyValue) > 1 AND HCM_VisitPlan.ClientRef = '" . $clientToken . "'
        
        return $query->result();
    }

    /*
     * Update Notification Status
     * Data: userID, deviceID
     */
    public function updateNotification($data)
    {
        $query = $this->db->query("UPDATE dbo.users_devices SET notification = '" . $data['notification'] . "' WHERE userID = '" . $data['userToken'] . "' AND deviceID = '" . $data['deviceID'] . "'");
        /**
        * Logs
        */
        $notif = ($data['notification']) ? "1" : "0";
        $this->Log_model->add_log("update_notification", $data['userToken'], $notif, $data['deviceID']);
        return $query;
    }

    public function getNotification($userToken)
    {
        // $query = $this->db->query("SELECT NotificationID, UserID, Title, Message, CreatedAt AS Date, IsAccepted FROM dbo.Notification WHERE UserID = '" . $userToken . "' AND IsDeleted = 0 AND CreatedAt BETWEEN CAST(GETDATE() AS DATE) AND CAST(DATEADD(d, 30, GETDATE()) AS DATE) ORDER BY CreatedAt DESC");
        // $query = $this->db->query("SELECT NotificationID, UserID, Title, Message, CreatedAt AS Date, IsAccepted FROM dbo.Notification WHERE UserID = '" . $userToken . "' AND IsDeleted = 0 ORDER BY CreatedAt DESC");
        $date_q = date("Y-m-d", strtotime("-30 days"));
        $query = $this->db->query("SELECT NotificationID, UserID, Title, Message, CreatedAt AS Date, IsAccepted FROM dbo.Notification WHERE UserID = '" . $userToken . "' AND IsDeleted = 0 AND IsAccepted = 0 AND CreatedAt >= '" . $date_q . "' ORDER BY CreatedAt DESC");
        $result = $query->result();
        foreach ($result as $key => $value) {
            if ($value->IsAccepted == 1) {
                $result[$key]->isAccepted = 1;
            }
            else {
                $result[$key]->isAccepted = 0;
            }

            if ($this->isNotifContacted($value->NotificationID)) {
                $result[$key]->isContacted = 1;
            }
            else {
                $result[$key]->isContacted = 0;
            }
            $result[$key]->Date = date("d-m-Y g:i a", strtotime($value->Date));
        }
        /**
        * Logs
        */
        $this->Log_model->add_log("view_notification", $userToken);
        return $result;
    }

    /*
     * Check if Notif is accepted
     */
    public function isNotifAccepted($notificationID)
    {
        $query = $this->db->query("SELECT COUNT(*) AS notifCount FROM dbo.Messages WHERE NotificationID = '" . $notificationID . "' AND Type = 'reply'");
        return $query->result()[0]->notifCount;
    }

    /*
     * Check if Notif is contacted
     */
    public function isNotifContacted($notificationID)
    {
        $query = $this->db->query("SELECT COUNT(*) AS notifCount FROM dbo.Messages WHERE NotificationID = '" . $notificationID . "' AND Type = 'contact'");
        return $query->result()[0]->notifCount;
    }

    /*
     * Remove Notification from list
     */
    public function removeNotification($data)
    {
        $query = $this->db->query("UPDATE dbo.Notification SET IsDeleted = 1 WHERE NotificationID = '" . $data['NotificationID'] . "'");
        return $query;
    }
    
    /*
     * Accept Notification from list
     */
    public function acceptNotification($data)
    {
        $query = $this->db->query("UPDATE dbo.Notification SET IsAccepted = 1 WHERE NotificationID = '" . $data['NotificationID'] . "'");
        return $query;
    }

    /*
     * Get Total Notification that the user did not reply to or contact
     */
    public function getNumberOfUnreadNotif($userToken)
    {
        $date_q = date("Y-m-d", strtotime("-30 days"));
        // $query = $this->db->query("SELECT NotificationID, UserID, Title, Message, CreatedAt, IsAccepted AS Date FROM dbo.Notification WHERE UserID = '" . $userToken . "' AND IsDeleted = 0");
        $query = $this->db->query("SELECT NotificationID, IsAccepted FROM dbo.Notification WHERE UserID = '" . $userToken . "' AND IsDeleted = 0 AND CreatedAt >= '" . $date_q . "'");
        $result = $query->result();
        $NotifCount = 0;
        foreach ($result as $key => $value) {
            if ($value->IsAccepted != 1 && !$this->isNotifContacted($value->NotificationID)) {
                $NotifCount++;
            }
        }

        return $NotifCount;
    }

    public function sendMessage($data)
    {
        $CreatedAt = date("Y-m-d H:i:s");
        if (@$data['notificationID']) {
            $query = $this->db->query("INSERT INTO dbo.Messages (Message, Type, CreatedAt, UserID, NotificationID) VALUES ('" . htmlspecialchars($data['message'], ENT_QUOTES) . "', '" . $data['type'] . "', '" . $CreatedAt . "', '" . $data['userToken'] . "', '" . $data['notificationID'] . "')");
        }
        else {
            $query = $this->db->query("INSERT INTO dbo.Messages (Message, Type, CreatedAt, UserID) VALUES ('" . htmlspecialchars($data['message'], ENT_QUOTES) . "', '" . $data['type'] . "', '" . $CreatedAt . "', '" . $data['userToken'] . "')");
        }
        /**
        * Logs
        */
        $this->Log_model->add_log("send_message", $data['userToken'], $data['type']);
        return $query;
    }

    /*
     * Other?
     * Get Registered Mobile Number in users table
     * Used in validate PIN and register PIN
     */
    public function getRegisteredMobileNumber($userToken)
    {
        /**
         * Get Type (Carer / Client)
         */
        $query = $this->db->query("SELECT type, carerID, clientID FROM dbo.Users WHERE userID = '" . $userToken . "'");
        $user = $query->result()[0];

        if ($user->type == "Carer") {
            $query = $this->db->query("SELECT Telephone1, Telephone2, Telephone3 FROM dbo.HCM_Contact WHERE ContactID = '" . $user->carerID . "'");
            $res = $query->result()[0];
            $numbers[] = $res->Telephone1;
            $numbers[] = $res->Telephone2;
            $numbers[] = $res->Telephone3;
            $validNum = $this->validPhoneNumber($numbers);
        }
        elseif ($user->type == "Client") {
            $query = $this->db->query("SELECT Telephone1, Telephone2, Telephone3 FROM dbo.HCM_Client WHERE ClientID = '" . $user->clientID . "'");
            $res = $query->result()[0];
            $numbers[] = $res->Telephone1;
            $numbers[] = $res->Telephone2;
            $numbers[] = $res->Telephone3;
            $validNum = $this->validPhoneNumber($numbers);
        }

        return $validNum;
    }

    /**
     * Check if Phone Numbers are valid
     * starts with "04" and 10 digits
     * @param  [array] $phone_numbers [phone numbers registered in HCM database]
     * @return valid phone number or false if there's no valid phone number
     */
    public function validPhoneNumber($phone_numbers)
    {
        foreach ($phone_numbers as $phone_number) {
            $cleaned_phone_number = $this->cleanPhoneNumber($phone_number);
            if (substr($cleaned_phone_number, 0, 2) == "04" && strlen($cleaned_phone_number) == 10) {
                return $cleaned_phone_number;
            }
        }
        return false;
    }

    /**
     * remove spaces, periods, dashes
     * @param  [string] $phone_number
     * @return clean phone number
     */
    public function cleanPhoneNumber($phone_number)
    {
        # remove spaces
        $phone_number = str_replace(" ", "", $phone_number);

        # remove periods
        $phone_number = str_replace(".", "", $phone_number);

        # remove commas
        $phone_number = str_replace(",", "", $phone_number);

        # remove dashes
        $phone_number = str_replace("-", "", $phone_number);

        return $phone_number;
    }

    public function updateUserMobile($mobile, $userToken)
    {
        $this->db->query("UPDATE dbo.Users SET Mobile = '" . $mobile . "' WHERE userID = '" . $userToken . "'");
    }

    public function getContactData($firstName = NULL, $lastName = NULL)
    {
        $query = $this->db->query("SELECT * FROM dbo.HCM_Contact WHERE FirstName = '" . $firstName . "' AND LastName = '" . $lastName . "'");
        return $query->result();
    }
    
    public function getClientData($firstName = NULL, $lastName = NULL)
    {
        $query = $this->db->query("SELECT * FROM dbo.HCM_Client WHERE FirstName = '" . $firstName . "' AND LastName = '" . $lastName . "'");
        return $query->result();
    }

    public function saveVerificationCode($userToken, $smsCode)
    {
        $this->db->query("UPDATE dbo.Users SET verificationCode = '" . $smsCode . "' WHERE userID = '" . $userToken . "'");
    }

    public function sendSMSVerificationCode($userToken)
    {
        // $mobile_no = $this->getRegisteredMobileNumber($userToken);
        // $mobile_no = "61401821545";
        $smsCode = generateRandomString(4);
        
        $this->saveVerificationCode($userToken, $smsCode);
        
        $msg = "Anglicare SMS Verification Code: " . $smsCode;
        // var_dump($this->getUserByUserId($userToken)); die();
        
        $mobile_no = $this->getRegisteredMobileNumber($userToken);
        // $mobile_no = "0401821545";

        # cUrl
        $curl_data['mobile_no'] = $mobile_no;
        $curl_data['msg'] = $msg;
        $url = "users/sendSMSCurl";
        $request_method = 'post';
        curl_api($url, $request_method, $curl_data);
        
        // SendMessage("CI00201765", "Caroline.Peig@Anglicare.org.au", "wlh7XCi5", $mobile_no, $msg);
        
        /**
         * Send to email for testing
         */
        // mail("jcandres@myoptimind.com", "Anglicare SMS Verification Code", "Verification Code: $smsCode");
        // $this->load->library('email');

        // $this->email->from('jcandres@myoptimind.com', 'Anglicare');
        // $this->email->to('jcandres@myoptimind.com');

        // $this->email->subject('Anglicare SMS Verification Code');
        // $this->email->message('Verification Code: ' . $smsCode);

        // $this->email->send();
        return true;
    }

    public function verifyCode($userToken, $code)
    {
        $query = $this->db->query("SELECT COUNT(*) AS codeCount FROM dbo.Users WHERE userID = '" . $userToken . "' AND verificationCode = '" . $code . "'");
        return $query->result()[0]->codeCount;
    }
}