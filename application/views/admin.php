<?php
// var_dump($admins); die();
?>
<!--main content start-->
<section id="main-content">
    <section class="wrapper">
        <!-- page start-->

        <!-- table start -->
        <div class="row">
            <div class="col-sm-12">
                <!--breadcrumbs start -->
                <ul class="breadcrumb">
                    <li class="active">Admin</li>
                </ul>
                <!--breadcrumbs end -->
                <section class="panel">
                    <header class="panel-heading">
                        <?php //echo uri_string(); ?>

                        <h4 class="panel-title pull-right">
                            <a class="btn btn-info mtop20" data-toggle="modal" href="#addAdmin">Add</a>
                        </h4>

                        <span style="font-size: 14px; color: <?php echo $this->session->flashdata('alert_color'); ?>">
                            <?php echo $this->session->flashdata('alert_msg'); ?>
                        </span>
                        
                    </header>
                    <div class="panel-body">
                        <div class="adv-table">
                            <table class="display table table-bordered table-striped" id="dynamic-table">
                                <thead>
                                    <tr>
                                        <th>#</th>
                                        <th>Name</th>
                                        <th>Username</th>
                                        <th>Email</th>
                                        <th>Date Created</th>
                                        <th>Actions</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php
                                    $i = 1;
                                    foreach ($admins as $admin) {
                                    ?>
                                    <tr>
                                        <td><?php echo $i++; ?></td>
                                        <td><?php echo $admin->LastName . ", " . $admin->FirstName; ?></td>
                                        <td><?php echo $admin->UserName; ?></td>
                                        <td><?php echo $admin->Email; ?></td>
                                        <td><?php echo $admin->CreatedAt; ?></td>
                                        <td>
                                            <a href="#editAdmin" class="edit-admin" data-toggle="modal" 
                                            data-id="<?php echo $admin->AdminID; ?>"
                                            data-lname="<?php echo $admin->LastName; ?>"
                                            data-fname="<?php echo $admin->FirstName; ?>"
                                            data-email="<?php echo $admin->Email; ?>"
                                            data-username="<?php echo $admin->UserName; ?>"
                                            data-password="<?php echo $admin->Password; ?>"
                                            >Edit</a> / 
                                            <a href="#changePassword" class="change-password" data-toggle="modal"
                                            data-id="<?php echo $admin->AdminID; ?>"
                                            data-username="<?php echo $admin->UserName; ?>"
                                            >Change Password</a> / 
                                            <a href="javascript:void(0);" onclick="deleteAdmin('<?php echo $admin->AdminID; ?>')">Delete</a>
                                        </td>
                                    </tr>
                                    <?php
                                    }
                                    ?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </section>
            </div>
        </div>
        <!-- table end -->

        <!-- add admin modal start -->
        <div class="modal fade " id="addAdmin" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">
                    <form method="POST" action="<?php echo base_url('admin/addAdmin'); ?>">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                            <h4 class="modal-title">Add Admin</h4>
                        </div>
                        <div class="modal-body">
                            <div class="form-group">
                                <label>First Name</label>
                                <input type="text" class="form-control" name="fname" required>
                            </div>
                            <div class="form-group">
                                <label>Last Name</label>
                                <input type="text" class="form-control" name="lname" required>
                            </div>
                            <div class="form-group">
                                <label>Email Address</label>
                                <input type="email" class="form-control" name="email" required>
                            </div>
                            <div class="form-group">
                                <label>Username</label>
                                <input type="text" class="form-control" name="username" required>
                            </div>
                            <div class="form-group">
                                <label>Password</label>
                                <input type="password" class="form-control" name="password" required>
                            </div>
                            <div class="form-group">
                                <label class="control-label">Admin Level</label>
                                <div>
                                    <select class="form-control m-bot15" name="admin_level">
                                        <option value="2">Admin</option>
                                        <option value="3">Super Admin</option>
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="modal-footer">
                            <input type="hidden" id="admin_id" name="admin_id">
                            <button data-dismiss="modal" class="btn btn-default" type="button">Close</button>
                            <button class="btn btn-success" type="submit">Save changes</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
        <!-- add admin modal end -->

        <!-- edit admin modal start -->
        <div class="modal fade " id="editAdmin" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">
                    <form method="POST" action="<?php echo base_url('admin/updateAdmin'); ?>">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                            <h4 class="modal-title">Edit Admin</h4>
                        </div>
                        <div class="modal-body">
                            <div class="form-group">
                                <label>First Name</label>
                                <input type="text" class="form-control" id="fname" name="fname" required>
                            </div>
                            <div class="form-group">
                                <label>Last Name</label>
                                <input type="text" class="form-control" id="lname" name="lname" required>
                            </div>
                            <div class="form-group">
                                <label>Email Address</label>
                                <input type="text" class="form-control" id="email" name="email" required>
                            </div>
                            <div class="form-group">
                                <label>Username</label>
                                <input type="text" class="form-control" id="username" name="username" required>
                            </div>
                        </div>
                        <div class="modal-footer">
                            <input type="hidden" id="admin_id" name="admin_id">
                            <button data-dismiss="modal" class="btn btn-default" type="button">Close</button>
                            <button class="btn btn-success" type="submit">Save changes</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
        <!-- edit admin modal end -->

        <!-- change password modal -->
        <div class="modal fade " id="changePassword" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">
                    <form method="POST" action="<?php echo base_url('admin/changePassword'); ?>">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                            <h4 class="modal-title">Change Password</h4>
                        </div>
                        <div class="modal-body">
                            <div class="form-group">
                                <label>Old Password</label>
                                <input type="password" class="form-control" name="old_password" required>
                            </div>
                            <div class="form-group">
                                <label>New Password</label>
                                <input type="password" class="form-control" name="new_password" required>
                            </div>
                        </div>
                        <div class="modal-footer change-pass">
                            <input type="hidden" id="admin_id" name="admin_id">
                            <input type="hidden" id="username" name="username">
                            <button data-dismiss="modal" class="btn btn-default" type="button">Close</button>
                            <button class="btn btn-success" type="submit">Save changes</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
        <!-- change password modal END -->

        <!-- page end-->
    </section>
</section>
<!--main content end -->

<script type="text/javascript">
    $(function(){
        $(".edit-admin").on("click", function(){
            $(".modal-footer #admin_id").val($(this).data("id"));
            $(".modal-body #fname").val($(this).data("fname"));
            $(".modal-body #lname").val($(this).data("lname"));
            $(".modal-body #email").val($(this).data("email"));
            $(".modal-body #username").val($(this).data("username"));
            // $(".modal-body #password").val($(this).data("password"));
        });

        $(".change-password").on("click", function(){
            $(".modal-footer #admin_id").val($(this).data("id"));
            $(".modal-footer #username").val($(this).data("username"));
        });
    });

    function deleteAdmin(id) {
        if (confirm("Are you sure you want to delete this user?")) {
            window.location = "<?php echo base_url(); ?>"+"/admin/deleteAdmin/"+id;
        }
    }
</script>