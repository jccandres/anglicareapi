<!--main content start-->
<section id="main-content">
    <section class="wrapper">
    	<!-- page start-->

    	<!-- table start -->
        <div class="row">
            <div class="col-sm-12">
            	<!--breadcrumbs start -->
                <ul class="breadcrumb">
                    <li class="active">Logs</li>
                </ul>
                <!--breadcrumbs end -->

                <section class="panel">
                	
                	<header class="panel-heading">
                		<!-- <span style="font-size: 14px; color: <?php echo $this->session->flashdata('alert_color'); ?>">
                            <?php echo $this->session->flashdata('alert_msg'); ?>
                        </span> -->
                	</header>

                	<div class="panel-body">
                		<div class="adv-table">

                            <?php if (@$_GET['dev']): ?>
                                <div class="form-group">
                                    <div class="col-md-4">
                                        <div class="input-group input-large" data-date="13/07/2013" data-date-format="mm/dd/yyyy">
                                            <input type="text" class="form-control dpd1" name="from">
                                            <span class="input-group-addon">To</span>
                                            <input type="text" class="form-control dpd2" name="to">
                                        </div>
                                        <span class="help-block">Select date range</span>
                                    </div>
                                </div>
                            <?php endif ?>

                            <table class="display table table-bordered table-striped" id="dynamic-table">
                            	<thead>
                                    <tr>
                                        <th>Name</th>
                                        <th>Action</th>
                                        <th>Status</th>
                                        <th>Device ID</th>
                                        <th>Date</th>
                                    </tr>
                                </thead>
                                <tbody>
                                	<?php
                                	foreach ($logs as $data) {
                                	?>
                                	<tr>
                                		<td><?php echo $data->Name; ?></td>
                                		<td><?php echo $data->Type; ?></td>
                                		<td>
                                			<?php
                                				echo ($data->Status) ? $data->Status : "-";
                                			?>		
                                		</td>
                                		<td>
                                			<?php
                                				echo ($data->DeviceID) ? $data->DeviceID : "-";
                                			?>
                                		</td>
                                		<td><?php echo $data->CreatedAt; ?></td>
                                	</tr>
                                	<?php
                                	}
                                	?>
                                </tbody>
                            </table>
                        </div>
                    </div>

                </section>





            </div>
        </div>
        <!-- table end -->

    	<!-- page end-->
    </section>
</section>
<script type="text/javascript">
// $(function(){
$( "th:nth-last-child(1)" ).click();
$( "th:nth-last-child(1)" ).click();
// });
</script>