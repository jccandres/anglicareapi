<?php
// var_dump($users); die();
?>


<!--main content start-->
<section id="main-content">
    <section class="wrapper">
        <!-- page start-->
        <div class="row">
            <div class="col-lg-12">

                <!--breadcrumbs start -->
                <ul class="breadcrumb">
                    <li class="active">Notification</li>
                </ul>
                <!--breadcrumbs end -->

                <section class="panel">
                    <header class="panel-heading">
                        <?php //echo uri_string(); ?>

                        <span style="font-size: 14px; color: <?php echo $this->session->flashdata('alert_color'); ?>">
                            <?php echo $this->session->flashdata('alert_msg'); ?>
                        </span>

                    </header>
                    <div class="panel-body">
                        <form method="POST" action="<?php echo base_url('admin/sendNotification'); ?>">
                            <div class="form-group col-sm-7">
                                <label class="control-label">Users</label>
                                <div>
                                    <input list="users" id="user" name="userTemp">
  									<datalist id="users">
                                	<?php foreach ($users as $user) { ?>
                                        <option value="<?php echo trim($user->FirstName) . " " . trim($user->LastName) . " (". $user->type . " account)"; ?>">
                                    <?php } ?>
                                    </datalist>
                                </div>
                            </div>
                            <div class="form-group col-sm-7">
                                <label>Title</label>
                                <input type="text" class="form-control" name="title">
                            </div>
                            <div class="form-group col-sm-7">
                                <label>Message</label>
                                <textarea class="form-control" name="message" rows="5" maxlength="500"></textarea>
                            </div>
                            <div class="form-group col-sm-7">
                            	<input name="user" type="hidden" id="userId" value="">
                                <input type="submit" class="btn btn-info" name="" value="Send Message">
                            </div>

                        </form>
                    </div>
                </section>
            </div>
        </div>
        <!-- page end-->
    </section>
</section>
<!--main content end-->

<!-- JP edit start -->
<script>
	
    	
    	$("#user").change(function() {
  			var selectedUser = $('#user').val();
			
			var users = [];
			<?php foreach ($users as $user) { ?>
				users["<?php echo trim($user->FirstName) . " " . trim($user->LastName). " (". $user->type . " account)"; ?>"] = "<?php echo $user->userID; ?>";
			<?php } ?>
    	
    		//alert(users[selectedUser]);
    	
    		if(users[selectedUser] === undefined){
     			alert("Unknown user selected.");
     		}else{
     			$('#userId').val(users[selectedUser]);
     		}
		});

 
</script>
<!-- JP edit end -->