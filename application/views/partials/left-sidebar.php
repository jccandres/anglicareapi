<!--sidebar start-->
<aside>
  <div id="sidebar"  class="nav-collapse ">
    <!-- sidebar menu start-->
    <ul class="sidebar-menu" id="nav-accordion">

      <?php //if ($_SESSION['admin_level'] == 3): ?>
      <li>
        <a class="<?php echo (uri_string() == "admin/admin") ? 'active' : ''; ?>" href="<?php echo base_url('admin/admin'); ?>"> <i class="fa fa-users"></i> <span>Admin</span> </a>
      </li>
      <?php //endif ?>

      <li>
        <a class="<?php echo (uri_string() == "admin/notifications") ? 'active' : ''; ?>" href="<?php echo base_url('admin/notifications'); ?>"> <i class="fa fa-puzzle-piece"></i> <span>Notifications</span> </a>
      </li>

      <li>
        <a class="<?php echo (uri_string() == "admin/logs") ? 'active' : ''; ?>" href="<?php echo base_url('admin/logs'); ?>"> <i class="fa fa-puzzle-piece"></i> <span>Logs</span> </a>
      </li>
      
    </ul>
    <!-- sidebar menu end-->
  </div>
</aside>
<!--sidebar end-->